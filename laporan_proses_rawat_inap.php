<?php
	include "koneksi.php";

?>
<?php
$nota = isset($_GET['nota']) ? $_GET['nota'] : "";
$datapasienquery = mysql_query("SELECT * FROM tb_pemilik AS tp
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
LEFT JOIN tb_histori_rawat_inap AS th ON tp.code_pemilik=th.code_pemilik
WHERE th.nota='".$nota."'");
$datapasien = mysql_fetch_array($datapasienquery);
$jenis = "-";
$dokter = "";
if(isset($datapasien['jenis_kelamin'])){
	if($datapasien['jenis_kelamin'] == '0'){
		$jenis = "Betina";
	}else if($datapasien['jenis_kelamin'] == '1'){
		$jenis = "Jantan";
	}
}
$datadokterquery = mysql_query("SELECT * FROM rawat_jalan_dokter AS rjd
LEFT JOIN tb_dokter AS td ON rjd.dokter_id=td.dokter_id
WHERE rjd.nota='".$nota."'");
while($datadokter = mysql_fetch_array($datadokterquery)){
	if(empty($dokter)){
		$dokter .=$datadokter['dokter_name'];
	}else{
		$dokter .=", ".$datadokter['dokter_name'];
	}
}
if(empty($dokter)){
	$dokter = "-";
}

$nama_dokumen='Nota Pembayaran Rawat Jalan.pdf'; //Beri nama file PDF hasil.
define('_MPDF_PATH','MPDF/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4');
$w = 24*10;
$h = 14*10;
$mpdf=new mPDF('utf-8',array($w,$h)); // Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body onload="">
<style>
	.clearfix{
		clear:both;
	}
	.td{
		padding:2px;
		border:1px solid #000;
	}
</style>
<div id="header" style=" font-size:10px;">
		<div style="width:100%;padding:5px;">
			<p align="center">
    		<img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/><br>
				Jl. LC Intan II / Gang X No.1, Denpasar Utara | 081 916 159 276
				<hr />
			</p>
    </div>
</div>
<div id="conten">
	<table cellpadding="0" cellspacing="0" border="0" style="width:100%;font-size:8px;border:1px solid #000;">
		<thead>
				<tr>
					<th class="td">No</th>
					<th class="td">Nota</th>
					<th class="td">Nama Pemilik</th>
					<th class="td">Alamat</th>
					<th class="td">Nama Pasien</th>
					<th class="td">Tgl Berobat</th>

					<th class="td">Berat</th>
					<th class="td">Temp</th>
					<th class="td">Crt</th>
					<th class="td">Status Vaksin</th>
					<th class="td">Keterangan</th>
					<th class="td">Tgl Titip</th>
					<th class="td">Tgl Ambil</th>
					<th class="td">Biaya</th>
				</tr>
			</thead>
		<tbody>
			<?php
			$n= 1;
			$tgl_awal = date('Y-m-d', strtotime($_GET['tanggal_awal']));
			$tgl_awal = $tgl_awal.' 00:01:01';

			$tgl_akhir = date('Y-m-d',strtotime($_GET['tanggal_akhir']));
			$tgl_akhir = $tgl_akhir.' 23:00:00';
			//get all berobat
			$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik inner join tb_histori_rawat_inap as c on a.code_pemilik = c.code_pemilik
				where
				(c.tgl_berobat BETWEEN '$tgl_awal' AND '$tgl_akhir')
				order by c.tgl_berobat desc");
			$total = 0;
			$pasien = 0;
			while($foo = mysql_fetch_array($res)){
				$total += $foo['a_biaya'];
				$pasien += 1;
			?>
				<tr>
					<td class="td"><?php echo $n++;?></td>
					<td class="td"><?php echo $foo['nota'];?></td>
					<td class="td"><?php echo $foo['nama_pemilik'];?></td>
					<td class="td"><?php echo $foo['alamat_pemilik']; ?></td>
					<td class="td"><?php echo $foo['nama_hewan'];?></td>
					<td class="td"><?php echo $foo['tgl_berobat']; ?></td>

					<td class="td"><?php echo $foo['a_berat'].'Kg';?></td>
					<td class="td"><?php echo $foo['a_temp'];?></td>
					<td class="td"><?php echo $foo['a_crt']; ?></td>
					<td class="td"><?php echo $foo['a_status_vaksin'];?></td>
					<td class="td"><?php echo $foo['a_keterangan']; ?></td>
					<td class="td"><?php echo $foo['tgl_titip']; ?></td>
					<td class="td"><?php echo $foo['tgl_ambil']; ?></td>
					<td class="td"><?php echo "Rp.".number_format($foo['a_biaya']); ?></td>

				</tr>
			<?php
				}
			?>
			</tr>
		</tbody>
    </table>
		<table cellspacing="0" collpadding="0" style="width: 90%;   font-size:10px;" border="solid 1px #ddd">
			<tr>
					<td>Total Pasien</td>
					<td>Total Berobat</td>
				</tr>
				<tr>
						 <td><?php echo number_format($pasien);?></td>
						 <td><?php echo "Rp. ".number_format($total);?></td>
				</tr>

		</table>
		<hr />
		<p style="font-size:10px;">Periode Laporan Berobat : <?php echo $_GET['tanggal_awal'].' s/d '.$_GET['tanggal_akhir'];?></p>
    <hr />
</div>
<div class="clearfix"></div>
<div id="footer">

</div>
</body>

</html>
<?php

$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');


exit;
?>
<!--script>
		window.print();
		//window.close();
</script-->
