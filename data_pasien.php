<?php
session_start();
include "koneksi.php";


include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">

			<!---form data---->
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2> Data Pasien</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a href='javascript:'><button type="button" class="btn btn-success" id="search"><i class='fa fa-search'></i> Pencarian</button></a></li>
				 <li><a href="form_pasien.php"><input type="button" class="btn btn-primary" value="Pasien baru"></a></li>
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>

				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">

				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				  <thead>
					<tr>
					  <th>ID Pasien</th>
            <th>No Pasien</th>
					  <th>Nama Pemilik</th>
					  <th>Alamat</th>
					  <th>No tlp</th>
            <th>Nama Pasien</th>
					  <th>Jenis</th>
            <!--th><i class="fa fa-cogs"></i></th>
            <th><i class="fa fa-cogs"></i></th-->
					  <th><i class="fa fa-cogs"></i></th>
            <th><i class="fa fa-cogs"></i></th>
            <th><i class="fa fa-cogs"></i></th>
            <th><i class="fa fa-cogs"></i></th>
            <th><i class="fa fa-cogs"></i></th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$n= 1;
						//get all users
						$where = "";
						if(isset($_POST['pasien_name'])){
							if(empty($where)){
								$where .="b.nama_hewan LIKE '%".$_POST['pasien_name']."%'";
							}else{
								$where .=" AND b.nama_hewan LIKE '%".$_POST['pasien_name']."%'";
							}
						}
						if(!empty($where)){
							$where = " WHERE ".$where;
						}
						$res = mysql_query("select * from tb_pemilik as a left join tb_pasien as b on a.code_pemilik = b.code_pemilik $where order by b.id_counter_pasien ASC");
						while($foo = mysql_fetch_array($res)){
					?>
						<tr>
						  <td><?php echo $foo['id_counter_pasien'];?></td>
              <td><?php echo $foo['code_pemilik'];?></td>
						  <td><?php echo $foo['nama_pemilik'];?></td>
						  <td><?php echo $foo['alamat_pemilik']; ?></td>
						  <td><?php echo $foo['no_tlp']; ?></td>
              <td><?php echo $foo['nama_hewan'];?></td>
              <td><?php echo $foo['jenis']; ?></td>
              <!--td><a href="form_berobat.php?code_pemilik= echo $foo['code_pemilik']; &type=edit_berobat"><u><i class="fa fa-plus"></i> Berobat</u></a></td>
              <td><a href="form_grooming.php?code_pemilik= echo $foo['code_pemilik']; &type=edit_grooming"><u><i class="fa fa-plus"></i> Grooming</u></a></td-->
              <td><a href="form_rawat_sehat.php?code_pemilik=<?php echo $foo['code_pemilik']; ?>&type=new_rawat_sehat"><u><i class="fa fa-plus"></i> Rawat Sehat</u></a></td>
              <td><a href="form_rawat_sakit.php?code_pemilik=<?php echo $foo['code_pemilik']; ?>&type=new_rawat_sakit"><u><i class="fa fa-plus"></i> Rawat Sakit</u></a></td>
              <td><a href="form_rawat_inap.php?code_pemilik=<?php echo $foo['code_pemilik']; ?>&type=new_rawat_inap"><u><i class="fa fa-plus"></i> Rawat Inap</u></a></td>
						  <td><a href="form_pasien.php?id_pasien=<?php echo $foo['id_pasien']; ?>&type=edit_pasien"><u><i class="fa fa-pencil"></i> Edit</u></a></td>
						  <td><a href="print_pemeriksaan_pasien.php?code_pemilik=<?php echo $foo['code_pemilik']; ?>"><u><i class="fa fa-print"></i> Cetak Rekam Medis</u></a></td>
						</tr>
					<?php
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
		  </div>
			<!--form data--->
	</div>
</div><!--page content-->
<div class="modal fade" id="modalform" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	 <div class="modal-dialog modal-lg" role="document">
			 <div class="modal-content">
						<form id="detail" data-modal="#modalform" method="POST">
						<div class="modal-header">
								<h3 class="modal-title">Pencarian Data Pasien</h3>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12 col-lg-12">
									<div class="form-group form-group-default">
										<label for="user_name">Nama Pasien </label>
										<input type="text" name="pasien_name" class="form-control" placeholder="Isikan nama pasien" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
							<button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-search'></i> Cari</button>
						</div>
						</form>
				</div>
	 </div>
<!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Datatables -->
    <script>
      $(document).ready(function() {
				$("#search").click(function(){
					$("#modalform").modal("show");
				})
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
<?php
include "site_footer.php";
?>
