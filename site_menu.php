    <?php
		include "koneksi.php";


	?>
	<div class="col-md-3 left_col">

          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; background: none repeat scroll 0 0 #fff; ">
              <a href="index.php" class="site_title"><img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $_SESSION['username'];?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home </a></li>
                  <li><a><i class="fa fa-home"></i> Master <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="data_staff.php">Data User</a></li>
                      <li><a href="data_dokter.php">Data Dokter</a></li>
                      <li><a href="data_jasabarang.php">Data Jasa & Barang</a></li>
                    </ul>
                  </li>
                  <li style="background: #fc0078;"><a href="data_pasien.php"><i class="fa fa-user"></i>Data Pasien</a></li>
                  <li style="background: #8a6d3b;"><a href="data_jadwal.php"><i class="fa fa-calendar"></i> Jadwal </a></li>
                  <li style="background: red;display:none;"><a href="data_berobat.php"><i class="fa fa-user"></i> Berobat </a></li>
                  <li style="display:none;"><a href="data_grooming.php"><i class="fa fa-money"></i> Grooming </a></li>
                  <li style="background: green;"><a href="data_rawat_sehat.php"><i class="fa fa-money"></i> Rawat Sehat </a></li>
                  <li style="background: brown;"><a href="data_rawat_sakit.php"><i class="fa fa-money"></i> Rawat Sakit </a></li>
                  <li style="background: blue;"><a href="data_rawat_inap.php"><i class="fa fa-money"></i> Rawat Inap </a></li>
                  <li style="background: red;"><a href="data_pembayaran.php"><i class="fa fa-money"></i> Pembayaran </a></li>
                  <li><a href="from_laporan.php"><i class="fa fa-file"></i> Laporan </a></li>
                  <li><a href="about.php"><i class="fa fa-question-circle"></i> About </a></li>
                  <li style="background:#393f54;"><a href="logout.php"><i class="fa fa-power-off"></i> Keluar</a></li>

                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">


            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
			  <div class="nav navbar-nav navbar-right">
			  <h4 style="margin-right: 20px; text-align: right;"></h4></div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
