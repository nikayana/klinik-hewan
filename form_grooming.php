<?php 
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$username = '';
$password = '';
$msg_error = '';

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_grooming"){
	$mode = $_GET['type'];
	$code_pemilik = $_GET['code_pemilik'];

	$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik where a.code_pemilik = '$code_pemilik' ");
	$foo = mysql_fetch_assoc($res); 
	 
	$nama_pemilik = $foo['nama_pemilik'];
	$alamat_pemilik = $foo['alamat_pemilik'];
	$no_tlp = $foo['no_tlp'];
	$anamnesa = $foo['anamnesa'];
	$nama_hewan = $foo['nama_hewan'];
	$jenis = $foo['jenis'];
	$signalemen = $foo['signalemen'];  
	 
}

if(isset($_GET['type']) && $_GET['type'] == "hapus_grooming"){
	$mode = $_GET['type'];
	$nota = $_GET['nota'];
	$res = mysql_query("
		delete from tb_histori_grooming where nota = '$nota'
	");

	if($res){
		header('location: data_grooming.php');
	}else{
		$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik inner join tb_histori_grooming as c on a.code_pemilik = c.code_pemilik where c.nota = '$nota' ");
		$foo = mysql_fetch_assoc($res); 
		 
		$nama_pemilik = $foo['nama_pemilik'];
		$alamat_pemilik = $foo['alamat_pemilik'];
		$no_tlp = $foo['no_tlp'];
		$anamnesa = $foo['anamnesa'];
		$nama_hewan = $foo['nama_hewan'];
		$jenis = $foo['jenis'];
		$signalemen = $foo['signalemen']; 
		$msg_error .= 'Gagal hapus data berobat <br>';
 
	}
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	 
	//insert pasein baru
	$code_pemilik = $_POST['code_pemilik'];
	$alamat_pemilik = $_POST['alamat_pemilik'];
	$no_tlp = $_POST['no_tlp'];
	$anamnesa = $_POST['anamnesa'];
 	$nama_hewan = $_POST['nama_hewan'];
	$nama_hewan = $_POST['nama_hewan'];
	$jenis = $_POST['jenis'];
	$signalemen = $_POST['signalemen'];
	$tgl_update = date('Y-m-d H:i:s', strtotime($_POST['tgl_berobat']));
 	

	$berat = $_POST['berat'];
	$temp = $_POST['temp'];
	$crt = $_POST['crt'];
	$status_vaksinasi = $_POST['status_vaksinasi']; 
	$keterangan = $_POST['keterangan'];
	$biaya = $_POST['biaya']; 
	$nota = 'PD'.date('YmdHis');
	$res = mysql_query("
		insert into tb_histori_grooming
		(nota, tgl_berobat, code_pemilik,  a_berat, a_temp, a_crt, a_status_vaksin, a_keterangan, a_biaya)
		values 
		('$nota', '$tgl_update', '$code_pemilik', '$berat', '$temp', '$crt', '$status_vaksinasi', '$keterangan', '$biaya')
	");
	  
	if($res){
		header('location: data_grooming.php');
	}else{
		$msg_error .= 'gagal simpan data berobat <br>';
 
	}
}
 

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">
		  
		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Grooming</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_pasien.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Pemilik<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="nama_pemilik" required="required" class="form-control col-md-7 col-xs-12" name="nama_pemilik" value="<?php echo (isset($nama_pemilik)) ? $nama_pemilik : '' ;?>">
                      
                      <input type="hidden" id="code_pemilik"  class="form-control col-md-7 col-xs-12" name="code_pemilik" value="<?php echo (isset($code_pemilik)) ? $code_pemilik : '' ;?>">
                      
                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="alamat_pemilik" required="required" class="form-control col-md-7 col-xs-12" name="alamat_pemilik" value="<?php echo (isset($alamat_pemilik)) ? $alamat_pemilik : '' ;?>">
					</div>
				  </div> 
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Tlp<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="no_tlp" required="required" class="form-control col-md-7 col-xs-12" name="no_tlp" value="<?php echo (isset($no_tlp)) ? $no_tlp : '' ;?>">
					</div>
				  </div>
				   <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Anamnesa<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="anamnesa" required="required" class="form-control col-md-7 col-xs-12" name="anamnesa" value="<?php echo (isset($anamnesa)) ? $anamnesa : '' ;?>">
					</div>
				  </div> 
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Hewan<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="nama_hewan" required="required" class="form-control col-md-7 col-xs-12" name="nama_hewan" value="<?php echo (isset($nama_hewan)) ? $nama_hewan : '' ;?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="jenis" required="required" class="form-control col-md-7 col-xs-12" name="jenis" value="<?php echo (isset($jenis)) ? $jenis : '' ;?>">
					</div>
				  </div> 
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Signalemen<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="signalemen" required="required" class="form-control col-md-7 col-xs-12" name="signalemen" value="<?php echo (isset($signalemen)) ? $signalemen : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  	<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tgl Berobat<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <input type="text" id="tgl_berobat" required="required" class="form-control datapicker col-md-7 col-xs-12" name="tgl_berobat" value="<?php echo (isset($tgl_berobat)) ? $tgl_berobat : '' ;?>">
						</div>
					</div>
				  	<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Berat<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <input type="text" id="berat" required="required" class="form-control col-md-7 col-xs-12" name="berat" value="<?php echo (isset($berat)) ? $berat : '' ;?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Temp<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <input type="text" id="temp" required="required" class="form-control col-md-7 col-xs-12" name="temp" value="<?php echo (isset($temp)) ? $temp : '' ;?>">
						</div>
				  	</div>
					  <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Crt<span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="crt" required="required" class="form-control col-md-7 col-xs-12" name="crt" value="<?php echo (isset($crt)) ? $crt : '' ;?>">
							</div>
					  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Statu Vaksinasi<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="status_vaksinasi" required="required" class="form-control col-md-7 col-xs-12" name="status_vaksinasi" value="<?php echo (isset($status_vaksinasi)) ? $status_vaksinasi : '' ;?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Keterangan<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <textarea class="form-control" name="keterangan">
					  	
					  </textarea>
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Biaya<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="biaya" required="required" class="form-control col-md-7 col-xs-12" name="biaya" value="<?php echo (isset($biaya)) ? $biaya : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_pasien.php"><button type="button" class="btn btn-primary">Cancel</button></a>
					  <button type="submit" class="btn btn-success">Save</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php 
include "site_footer.php";
?>


<script type="text/javascript">
	$(document).ready(function(){
		$('.datapicker').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
	});
</script>