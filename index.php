<?php 
session_start();
//cek user login 
if ($_SESSION['username'] ==''){ header('location: login.php'); }
include "koneksi.php";

$data = array();
$search = "";

if(isset($_POST['pasien']) && strlen($_POST['pasien'])){
  $pasien = $_POST['pasien'];
  $search ="Rekam Medis Hewan";
  $rawatsehat = array();
  $rawatsakit = array();
  $rawatinap = array();
  $query = mysql_query("
    select * from tb_pemilik as a 
    inner join tb_pasien as b on a.code_pemilik = b.code_pemilik 
    inner join tb_histori_rawat_sehat as c on a.code_pemilik = c.code_pemilik 
    WHERE b.id_counter_pasien='".$_POST['pasien']."' 
    order by c.tgl_berobat desc
  "); 
  while ($foo = mysql_fetch_assoc($query)) {
    $rawatsehat[] = $foo;
  }

  $query = mysql_query("
    select * from tb_pemilik as a 
    inner join tb_pasien as b on a.code_pemilik = b.code_pemilik 
    inner join tb_histori_rawat_sakit as c on a.code_pemilik = c.code_pemilik 
    WHERE b.id_counter_pasien='".$_POST['pasien']."' 
    order by c.tgl_berobat desc
  "); 
  while ($foo = mysql_fetch_assoc($query)) {
    $rawatsakit[] = $foo;
  }

  $query = mysql_query("
    select * from tb_pemilik as a 
    inner join tb_pasien as b on a.code_pemilik = b.code_pemilik 
    inner join tb_histori_rawat_inap as c on a.code_pemilik = c.code_pemilik 
    LEFT JOIN tb_pemeriksaan AS tps2 ON c.nota=tps2.nota 
    WHERE b.id_counter_pasien='".$_POST['pasien']."' 
    order by c.tgl_berobat desc
  "); 
  while ($foo = mysql_fetch_assoc($query)) {
    $rawatinap[] = $foo;
  }  

}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class=""> 
		<div class="row"> 
      <div class="col-lg-6">
        <form class="form-vertical" method="post" action="">
          <div class="form-group">
            <label>Cari Nama Pasien</label>
            <select name="pasien" id="selectrekammedis2" style="width:100%">
            </select>
          </div>
          <div class="form-group">
            <button id="cari" type="submit" class="btn btn-danger" name="berobat" value="Rekam Medik">Cek Rekam Medik</button>
            <!--button id="cari" type="submit" class="btn btn-primary" name="grooming" value="Layanan Grooming">Layanan Grooming</button-->
          </div>
        </form>
      </div>
      <div class="clearfix"></div> 
      <div class="col-lg-12">
         <?php 
          if(!empty($rawatsehat) || !empty($rawatsakit) || !empty($rawatinap)){
            ?>
            <div class="x_panel">
              <div class="x_title">
                <h2>Rawat Sehat</h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
            <table class="table table-striped table-bordered dt-responsive nowrap datatable-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Jenis Pemeriksaan/Examination</th>
                <th>Pengobatan/Treatment</th>
                <th>Catatan/Note</th> 
                <th><i class="fa fa-cogs"></i></th> 
              </tr>
              </thead>
              <tbody>
              <?php
                foreach($rawatsehat AS $key => $value){
                  $obat = "";
                  $dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM rawat_sehat_obat AS tp
                  LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
                  WHERE tp.nota='".$value['nota']."'");
                  while($dataobat = mysql_fetch_array($dataobatquery)){
                    if(empty($obat)){
                      $obat .=$dataobat['jasa_barang_name'];
                    }else{
                      $obat .=", ".$dataobat['jasa_barang_name'];
                    }
                  }
                  if(empty($obat)){
                    $obat = "-";
                  }
              ?>
                <tr>
                  <td><?php echo $key+1;?></td> 
                  <td><?php echo isset($value['tgl_titip']) ? date("d F Y H:i:s", strtotime($value['tgl_titip'])) : ''; ?></td>
                  <td><?php echo $value['a_keterangan'];?></td>
                  <td><?php echo $obat;?></td>
                  <td>&nbsp;</td> 
    
                  <td>
                    <a href="form_rawat_sehat.php?nota=<?php echo $value['nota']; ?>&type=edit_rawat_sehat"><u><i class="fa fa-trash"></i> Edit Data</u></a><br>
                    <a href="form_rawat_sehat.php?nota=<?php echo $value['nota']; ?>&type=hapus_berobat"><u><i class="fa fa-trash"></i> Hapus Data</u></a>
                    <a href="print_bill_sehat.php?nota=<?php echo $value['nota'] ?>"><u><i class='fa fa-file-o'></i> Print Bill</u></a>
                  </td>
                </tr>
              <?php 
                }
              ?>
              </tbody>
              
            </table>
            </div>
          </div>
          <div class="x_panel">
			  <div class="x_title">
				<h2>Rawat Sakit</h2>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">

				<table class="table table-striped table-bordered dt-responsive nowrap datatable-responsive" cellspacing="0" width="100%">
				  <thead>
					  <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Jenis Pemeriksaan/Examination</th>
                <th>Pengobatan/Treatment</th>
                <th>Catatan/Note</th> 
                <th><i class="fa fa-cogs"></i></th> 
              </tr>
				  </thead>
				  <tbody>
					<?php
						foreach($rawatsakit AS $key => $value){
              $obat = "";
              $dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM rawat_jalan_obat AS tp
              LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
              WHERE tp.nota='".$value['nota']."'");
              while($dataobat = mysql_fetch_array($dataobatquery)){
                if(empty($obat)){
                  $obat .=$dataobat['jasa_barang_name'];
                }else{
                  $obat .=", ".$dataobat['jasa_barang_name'];
                }
              }
              if(empty($obat)){
                $obat = "-";
              }
					?>
						<tr>
						 <td><?php echo $key+1;?></td>
              <td><?php echo isset($value['tgl_titip']) ? date("d F Y H:i:s", strtotime($value['tgl_titip'])) : ''; ?></td>
              <td><?php echo $value['a_keterangan'];?></td>
						  <td><?php echo $obat;?></td>
						  <td>&nbsp;</td>
              <td>
                <a href="form_rawat_sakit.php?nota=<?php echo $value['nota']; ?>&type=edit_rawat_sakit"><u><i class="fa fa-trash"></i> Edit Data</u></a><br>
                <a href="form_rawat_sakit.php?nota=<?php echo $value['nota']; ?>&type=hapus_berobat"><u><i class="fa fa-trash"></i> Hapus Data</u></a>
								<a href="print_bill.php?nota=<?php echo $value['nota'] ?>"><u><i class='fa fa-file-o'></i> Print Bill</u></a>
              </td>
						</tr>
					<?php
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
      <div class="x_panel">
			  <div class="x_title">
				<h2>Rawat Inap</h2>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">

				<table class="table table-striped table-bordered dt-responsive nowrap datatable-responsive" cellspacing="0" width="100%">
				  <thead>
					  <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Jenis Pemeriksaan/Examination</th>
                <th>Pengobatan/Treatment</th>
                <th>Catatan/Note</th> 
                <th><i class="fa fa-cogs"></i></th> 
              </tr>
				  </thead>
				  <tbody>
					<?php
						foreach($rawatinap AS $key => $value){
              $obat = "";
              $dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM tb_pemeriksaan_obat AS tp
              LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
              WHERE tp.pemeriksaan_id='".$value['pemeriksaan_id']."'");
              
              while($dataobat = mysql_fetch_array($dataobatquery)){
                if(empty($obat)){
                  $obat .=$dataobat['jasa_barang_name'];
                }else{
                  $obat .=", ".$dataobat['jasa_barang_name'];
                }
              }
              if(empty($obat)){
                $obat = "-";
              }
					?>
						<tr>
						 <td><?php echo $key+1;?></td>
              <td><?php echo isset($value['pemeriksaan_date']) ? date("d F Y H:i:s", strtotime($value['pemeriksaan_date'])) : ''; ?></td>
              <td><?php echo $value['pemeriksaan_jenis'];?></td>
						  <td><?php echo $obat;?></td>
						  <td><?php echo $value['pemeriksaan_note'] ?></td>
              <td>
                <a href="form_rawat_inap.php?nota=<?php echo $value['nota']; ?>&type=edit_rawat_inap"><u><i class="fa fa-trash"></i> Edit Data</u></a><br>
                <a href="form_rawat_inap.php?nota=<?php echo $value['nota']; ?>&type=hapus_berobat"><u><i class="fa fa-trash"></i> Hapus Data</u></a>
								<a href="data_pemeriksaan.php?nota=<?php echo $value['nota'] ?>"><u><i class="fa fa-stethoscope"></i> Pemeriksaan</u></a>
								<a href="print_pemeriksaan.php?nota=<?php echo $value['nota'] ?>"><u><i class='fa fa-file-o'></i> Print Bill</u></a>
              </td>
						</tr>
					<?php
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
            <?php
          }else{
            echo "<img src='images/dokter.jpg'>";
          }
         ?>
      </div> 
  		<div class="clearfix"></div> 
  		<div></div> 
	  </div>
</div><!--page content-->
<!-- Datatables -->
<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('.datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
<?php 
include "site_footer.php";
?>
     

 