<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$username = '';
$password = '';
$msg_error = '';



include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Berobat</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_proses.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>

		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Grooming</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_proses_grooming.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>


		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Rawat Sehat</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_proses_rawat_sehat.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>

		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Rawat Sakit</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_proses_rawat_sakit.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>

    <div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Rawat Inap</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_proses_rawat_inap.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>

    <div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Pendapatan Jasa/Barang</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="laporan_pendapatan_jasabarang.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Awal<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tanggal_awal" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_awal" value="<?php //echo (isset($nama_admin)) ? $nama_admin : '' ;?>">

					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Akhir<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="tgl_akhir" required="required" class="form-control col-md-7 col-xs-12 datapicker" name="tanggal_akhir" value="<?php //echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Laporan Pasien</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form  action="print_pemeriksaan_pasien.php" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="GET">

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pasien<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					 	<select name="code_pemilik" class="form-control select2">
								<option value="">Pilih Pasien</option>
								<?php
								$res = mysql_query("select * from tb_pemilik as a left join tb_pasien as b on a.code_pemilik = b.code_pemilik $where order by b.id_counter_pasien ASC");
								while($foo = mysql_fetch_array($res)){
									?>
									<option value="<?php echo $foo['id_pasien']?>"><?php echo $foo['nama_hewan']." (Pemilik: ".$foo['nama_pemilik'].")"?></option>
									<?php
								}
								?>
						</select>

					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button type="submit" class="btn btn-success">Cetak</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datapicker').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
	});
</script>
