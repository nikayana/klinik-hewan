<?php
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aplikasi Rekam Medis Hewan</title>


    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	<!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/select2/dist/css/select2.css" rel="stylesheet">
    <link href="vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">

		<!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
   <!-- <script src="../vendors/Chart.js/dist/Chart.min.js"></script>

    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="vendors/Flot/jquery.flot.js"></script>
    <script src="vendors/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/Flot/jquery.flot.time.js"></script>
    <script src="vendors/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="vendors/flot.curvedlines/curvedLines.js"></script>

    <!-- JQVMap -->
    <script src="vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="vendors/moment/min/moment.min.js"></script>
    <script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="vendors/select2/dist/js/select2.js"></script>
    <script src="vendors/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $(".select2").select2();
        $("#jasabarang").change(function(){
          let getid = $(this).val();
          let getname = $("#jasabarang option:selected").text();
          let getprice = $("#jasabarang option:selected").data("price");

          let lastno = $("#listitem tr").length+1;
          let template = "<tr><td>"+lastno+"</td><td>"+getname+"<input type='hidden' name='jasa_barang_id[]' value='"+getid+"'></td><td><input type='text' name='jasa_barang_qty[]' value='1' class='form-control qty'></td><td>Rp "+addCommas(getprice)+"<input type='hidden' name='jasa_barang_price[]' class='price' value='"+getprice+"'></td><td><span class='subtotal'>Rp "+addCommas(getprice)+"</span></td><td><button class='btn btn-sm btn-danger del'><i class='fa fa-trash'></i></button></td></tr>";
          if(getid!=0){
            $("#listitem").append(template);
          }
          $(this).val(0);
          getotal();
        });

        $("#listitem").on("keyup", ".qty", function(){
            let price = $(this).closest("tr").find(".price").val();
            let qty = $(this).val();
            price = Number(price);
            let subtotal = price*qty;
            $(this).closest("tr").find(".subtotal").html("Rp "+addCommas(subtotal));
            getotal();

        });

        $("#listitem").on("click", ".del", function(){
            $(this).closest("tr").remove();
            getotal();
        });

        function getotal(){
          let price = $("#listitem").find(".price");
          let qty = $("#listitem").find(".qty");
          let subtotal = 0;
          for(i = 0; i < price.length; i++){
            subtotal += (Number(price.eq(i).val())*Number(qty.eq(i).val()));
          }
          $("#biaya").val(subtotal);
        }
        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
        $('.datetimepicker').datetimepicker();

        $("#selectrekammedis").select2({
          ajax: {
            url: 'data_select.php',
            dataType: "json",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: ""+item.pasien_id+" | "+item.name+" | "+item.nama_pemilik,
                            id: item.code_pemilik
                        }
                    })
                };
            }
          }
        });

        $("#selectrekammedis2").select2({
          ajax: {
            url: 'data_select.php',
            dataType: "json",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: ""+item.pasien_id+" | "+item.name+" | "+item.nama_pemilik,
                            id: item.pasien_id
                        }
                    })
                };
            }
          }
        });

        $(".selectpasien").change(function(){
          let getval = $(".selectpasien option:selected").text();
          $.ajax({
            type : "POST",
            url : 'data_select_tagihan.php',
            data : {
              pasien_id : getval
            },
            dataType : "json",
            success : function(data){
              $("#tbrawatsehat").html(data.rawatsehat);
              $("#tbrawatsakit").html(data.rawatsakit);
              $("#tbrawatinap").html(data.rawatinap);
              $("#pembayaran_billing").val(data.total);
              $("#pasien_id").val(data.pasien_id);
              kembalian();
            }
          });
        });

        $(".autocalc").keyup(function(){
          kembalian();
        })

        function kembalian(){
          let tagihan = $("#pembayaran_billing").val();
          let diskon = $("#pembayaran_diskon").val();
          let deposit = $("#pembayaran_deposit").val();
          let pembayaran = $("#pembayaran_pembayaran").val();
          let total = Number(tagihan) - Number(diskon) - Number(deposit);
          let kembali = (Number(diskon) + Number(deposit) + Number(pembayaran))-Number(tagihan);
          if(kembali < 0){
            kembali = 0;
          }

          if(total < 0){
            total = 0;
          }

          $("#pembayaran_kembalian").val(kembali);
          $("#sisa_tagihan").val(total);
          $("#pembayaran_total").val(total);
        }
      });

    </script>
	<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->

  </head>
<style>
	.menu_section ul.side-menu li :hover{ background:#74b2cd;}
	.menu_section ul.side-menu li ul li{ background:#2a3f54;}
	.menu_section ul.side-menu li ul li:hover{ background:#74b2cd;}
</style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
