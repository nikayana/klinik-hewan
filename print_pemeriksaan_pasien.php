<?php
	include "koneksi.php";
 
?>
<?php
$nota = isset($_GET['code_pemilik']) ? $_GET['code_pemilik'] : "";
$datapasienquery = mysql_query("SELECT * FROM tb_pemilik AS tp
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
LEFT JOIN tb_histori_rawat_inap AS th ON tp.code_pemilik=th.code_pemilik
LEFT JOIN tb_histori_rawat_sakit AS ths ON tp.code_pemilik=th.code_pemilik
LEFT JOIN tb_histori_rawat_sehat AS tha ON tp.code_pemilik=th.code_pemilik
WHERE tp.code_pemilik='".$nota."'");
$datapasien = mysql_fetch_array($datapasienquery);
$jenis = "-";
$dokter = "";
if(isset($datapasien['jenis_kelamin'])){
	if($datapasien['jenis_kelamin'] == '0'){
		$jenis = "Betina";
	}else if($datapasien['jenis_kelamin'] == '1'){
		$jenis = "Jantan";
	}
}
 

$datadokterquery = mysql_query("SELECT * FROM rawat_jalan_dokter AS rjd
LEFT JOIN tb_dokter AS td ON rjd.dokter_id=td.dokter_id
WHERE rjd.nota='".$nota."'");
while($datadokter = mysql_fetch_array($datadokterquery)){
	if(empty($dokter)){
		$dokter .=$datadokter['dokter_name'];
	}else{
		$dokter .=", ".$datadokter['dokter_name'];
	}
}
 
if(empty($dokter)){
	$dokter = "-";
}

$nama_dokumen='Nota Pembayaran Rawat Jalan.pdf'; //Beri nama file PDF hasil.
define('_MPDF_PATH','MPDF/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4');
$w = 14*10;
$h = 24*10;
$mpdf=new mPDF('utf-8',array($w,$h)); // Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rekam Medis</title>
</head>

<body onload="">
<style>
	.clearfix{
		clear:both;
	}
	.td{
		padding:2px;
		border:1px solid #000;
	}
</style>
<div id="header" style=" font-size:10px;">
		<div style="width:100%;padding:5px;">
			<p align="center">
    		    <img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/><br> 
			</p>
			<p align="center" style="margin-top:-5px;">Jl. LC Intan II / Gang X No.1, Denpasar Utara | Telp: 081 916 159 276 </p>
			<hr>
    </div>
</div>
<div id="conten">
    
	<table cellpadding=0 cellspacing=0 border=0 style="width:100%;font-size:10px;">
		<tbody>
				<tr>
					<td width="15%">No Pasien</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien[1]) ? $datapasien[1] : ""; ?><td>
					<td width="15%">Nama Hewan</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_hewan']) ? $datapasien['nama_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Nama</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_pemilik']) ? $datapasien['nama_pemilik'] : "" ?><td>
					<td width="15%">Spesies</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['jenis']) ? $datapasien['jenis'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Telepon</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['no_tlp']) ? $datapasien['no_tlp'] : "" ?><td>
					<td width="15%">Ras</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['ras_hewan']) ? $datapasien['ras_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Email</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['email']) ? $datapasien['email'] : "" ?><td>
					<td width="15%">Jenis</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo $jenis ?><td>
				</tr>
		</tbody>
	</table>
	<table cellspacing=0 cellpadding=0 class="table" style="width: 100%; font-size:8px;border:1px solid #000" border="0">
        <thead>
			<tr>
				<th class="td">Tanggal/<i>Date</i></th>
				<th class="td">Jenis Pemeriksaan/<i>Examination</i></th>
				<th class="td">Pengobatan/<i>Treatment</i></th>
				<!--th class="td">Catatan/<i>Note</i></th-->
			</tr>
        </thead>
        </tbody>
			<tr>
				<td class="td" colspan="3"><b>Rawat Sehat</b></td>
			</tr>
			<?php
			$n= 1;

			//get all berobat
			$res = mysql_query("SELECT * FROM tb_pemilik AS tp
			INNER JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
            LEFT JOIN tb_histori_rawat_sehat AS tha ON tp.code_pemilik=tha.code_pemilik 
			WHERE tp.code_pemilik='".$nota."'");

			while($foo = mysql_fetch_array($res)){
				$obat = "";
				$dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM rawat_sehat_obat AS tp
				LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
				WHERE tp.nota='".$foo['nota']."'");
				while($dataobat = mysql_fetch_array($dataobatquery)){
					if(empty($obat)){
						$obat .=$dataobat['jasa_barang_name'];
					}else{
						$obat .=", ".$dataobat['jasa_barang_name'];
					}
				}
				if(empty($obat)){
					$obat = "-";
				}
			?>
				<tr>
					<td class="td"><?php echo isset($foo['tgl_titip']) ? date("d F Y", strtotime($foo['tgl_titip'])) : ''; ?></td>
					<td class="td"><?php echo $foo['a_keterangan'];?></td>
					<td class="td" align="right"><?php echo $obat;?></td>
					<!--td class="td" align="right"></td-->

				</tr>
			<?php
				}
			?>
			<tr>
				<td class="td" colspan="3"><b>Rawat Sakit</b></td>
			</tr>
			<?php
			$n= 1;

			//get all berobat
			$res = mysql_query("SELECT * FROM tb_pemilik AS tp
			INNER JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
            LEFT JOIN tb_histori_rawat_sakit AS tha ON tp.code_pemilik=tha.code_pemilik 
			WHERE tp.code_pemilik='".$nota."'");

			while($foo = mysql_fetch_array($res)){
				$obat = "";
				$dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM rawat_jalan_obat AS tp
				LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
				WHERE tp.nota='".$foo['nota']."'");
				while($dataobat = mysql_fetch_array($dataobatquery)){
					if(empty($obat)){
						$obat .=$dataobat['jasa_barang_name'];
					}else{
						$obat .=", ".$dataobat['jasa_barang_name'];
					}
				}
				if(empty($obat)){
					$obat = "-";
				}
			?>
				<tr>
					<td class="td"><?php echo isset($foo['tgl_titip']) ? date("d F Y H:i:s", strtotime($foo['tgl_titip'])) : ''; ?></td>
					<td class="td"><?php echo $foo['a_keterangan'];?></td>
					<td class="td" align="right"><?php echo $obat;?></td>
					<!--td class="td" align="right"></td-->

				</tr>
			<?php
				}
			?>
			<tr>
				<td class="td" colspan="3"><b>Rawat Inap</b></td>
			</tr>
			<?php
			$n= 1;

			//get all berobat
			$res = mysql_query("SELECT * FROM tb_pemilik AS tp
			INNER JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
            LEFT JOIN tb_histori_rawat_inap AS tha ON tp.code_pemilik=tha.code_pemilik 
			LEFT JOIN tb_pemeriksaan AS tps2 ON tha.nota=tps2.nota 
			WHERE tp.code_pemilik='".$nota."'");
            
			while($foo = mysql_fetch_array($res)){
				$obat = "";
				$dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM tb_pemeriksaan_obat AS tp
				LEFT JOIN tb_jasabarang AS j ON tp.jasa_barang_id=j.jasa_barang_id
				WHERE tp.pemeriksaan_id='".$foo['pemeriksaan_id']."'");
				
				while($dataobat = mysql_fetch_array($dataobatquery)){
					if(empty($obat)){
						$obat .=$dataobat['jasa_barang_name'];
					}else{
						$obat .=", ".$dataobat['jasa_barang_name'];
					}
				}
				if(empty($obat)){
					$obat = "-";
				}
			?>
				<tr>
					<td class="td"><?php echo isset($foo['pemeriksaan_date']) ? date("d F Y H:i:s", strtotime($foo['pemeriksaan_date'])) : ''; ?></td>
					<td class="td"><?php echo $foo['pemeriksaan_jenis'];?></td>
					<td class="td" align="right"><?php echo $obat;?></td>
					<!--td class="td" align="right"><?php echo $foo['pemeriksaan_note'] ?></td-->

				</tr>
			<?php
				}
			?>
        </tbody>
    </table>

    <hr />
</div>
<div class="clearfix"></div>
<div id="footer">

</div>
</body>

</html>
<?php

$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');


exit;

?>
<!--script>
		window.print();
		//window.close();
</script-->
