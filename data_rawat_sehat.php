<?php 
session_start();
include "koneksi.php";


include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">
		  
		</div>
		<div class="clearfix"></div>
		<div class="row">
		  
			<!---form data---->
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2> Data Rawat Sehat</h2>
				<ul class="nav navbar-right panel_toolbox">
				 <li><a href="form_pasien.php?perawatan=rawatsehat"><input type="button" class="btn btn-primary" value="Pasien baru"></a></li>
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  
				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				
				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				  <thead>
					<tr>
					  <th>No</th>
            <th>Nota</th>
            <th>NO Pasien</th>
					  <th>Nama Pemilik</th>
					  <th>Alamat</th> 
            <th>Nama Pasien</th> 
            <th>Tgl Berobat</th>  

            <th>Berat</th>  
            <th>Temp</th>  
            <th>Status Vaksin</th> 
            <th>Tanggal Titip</th>
            <th>Pemeriksaan</th>
            <th>Biaya</th>  
					  <th><i class="fa fa-cogs"></i></th> 
					</tr>
				  </thead>
				  <tbody>
					<?php 
						$n= 1;
						//get all users
						$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik inner join tb_histori_rawat_sehat as c on a.code_pemilik = c.code_pemilik order by c.tgl_berobat desc");
						while($foo = mysql_fetch_array($res)){
					?>
						<tr>
						 <td><?php echo $n++;?></td> 
              <td><?php echo $foo['nota'];?></td>
              <td><?php echo $foo['code_pemilik'];?></td>
						  <td><?php echo $foo['nama_pemilik'];?></td>
						  <td><?php echo $foo['alamat_pemilik']; ?></td> 
              <td><?php echo $foo['nama_hewan'];?></td>
              <td><?php echo $foo['tgl_berobat']; ?></td> 

              <td><?php echo $foo['a_berat'].'Kg';?></td>
            <td><?php echo $foo['a_temp'];?></td>
              <td><?php echo $foo['a_status_vaksin'];?></td>
              <td><?php echo $foo['tgl_titip'];?></td>
              <td><?php echo $foo['a_keterangan']; ?></td> 
              <td><?php echo "Rp.".number_format($foo['a_biaya']); ?></td> 
 
              <td>
                <a href="form_rawat_sehat.php?nota=<?php echo $foo['nota']; ?>&type=edit_rawat_sehat"><u><i class="fa fa-trash"></i> Edit Data</u></a><br>
                <a href="form_rawat_sehat.php?nota=<?php echo $foo['nota']; ?>&type=hapus_berobat"><u><i class="fa fa-trash"></i> Hapus Data</u></a>
								<a href="print_bill_sehat.php?nota=<?php echo $foo['nota'] ?>"><u><i class='fa fa-file-o'></i> Print Bill</u></a>
              </td>
						</tr>
					<?php 
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
		  </div>
			<!--form data--->
	</div>
</div><!--page content-->
<!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
<?php 
include "site_footer.php";
?>
