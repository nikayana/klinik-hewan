<?php
session_start();
include "koneksi.php";


include "site_header.php";
include "site_menu.php";
$nota = isset($_GET['nota']) ? $_GET['nota'] : "";
?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">

			<!---form data---->
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2> Data Pemeriksaan</h2>
				<ul class="nav navbar-right panel_toolbox">
				 <li><a href="data_rawat_inap.php"><input type="button" class="btn btn-danger" value="Kembali"></a></li>
				 <li><a href="print_pemeriksaan.php?nota=<?php echo $nota ?>"><input type="button" class="btn btn-success" value="Cetak Nota"></a></li>
				 <li><a href="form_pemeriksaan.php?nota=<?php echo $nota ?>"><input type="button" class="btn btn-primary" value="Pemeriksaan Baru"></a></li>
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>

				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">

				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				  <thead>
					<tr>
					  <th>No</th>
            <th>Tgl Periksa</th>
            <th>Dokter</th>
					  <th>Jenis Pemeriksaan</th>
					  <th>Pengobatan</th>
            <th>Keterangan</th>
					  <th><i class="fa fa-cogs"></i></th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$n= 1;
						//get all users
						$res = mysql_query("select * from tb_pemeriksaan WHERE nota='$nota'");
						while($foo = mysql_fetch_array($res)){
						$dokter = "";
						$obat = "";

						$dataobatquery = mysql_query("SELECT j.jasa_barang_name, j.jasa_barang_id AS jasa_id, o.* FROM tb_pemeriksaan_obat AS o INNER JOIN tb_jasabarang AS j ON o.jasa_barang_id=j.jasa_barang_id WHERE pemeriksaan_id='".$foo['pemeriksaan_id']."'");
						while($dataobat = mysql_fetch_array($dataobatquery)){
							if(empty($obat)){
								$obat .=$dataobat['jasa_barang_name'];
							}else{
								$obat .=", ".$dataobat['jasa_barang_name'];
							}
						}

						$datadokterquery = mysql_query("SELECT j.dokter_name, o.* FROM tb_pemeriksaan_dokter AS o INNER JOIN tb_dokter AS j ON o.dokter_id=j.dokter_id WHERE pemeriksaan_id='".$foo['pemeriksaan_id']."'");
						while($datadokter = mysql_fetch_array($datadokterquery)){
							if(empty($dokter)){
								$dokter .=$datadokter['dokter_name'];
							}else{
								$dokter .=", ".$datadokter['dokter_name'];
							}
						}

						if(empty($dokter)){
							$dokter = "-";
						}
					?>
						<tr>
						 <td><?php echo $n++;?></td>
              <td><?php echo date("d F Y H:i:s", strtotime($foo['pemeriksaan_date']));?></td>
              <td><?php echo $dokter;?></td>
						  <td><?php echo $foo['pemeriksaan_jenis'];?></td>
						  <td><?php echo $obat; ?></td>
              <td><?php echo $foo['pemeriksaan_note'];?></td>
              <td>
                <a href="form_pemeriksaan.php?nota=<?php echo $_GET['nota']; ?>&type=edit_pemeriksaan&periksa=<?php echo $foo['pemeriksaan_id'] ?>"><u><i class="fa fa-trash"></i> Edit Data</u></a><br>
                <a href="form_pemeriksaan.php?nota=<?php echo $_GET['nota']; ?>&type=hapus_pemeriksaan&periksa=<?php echo $foo['pemeriksaan_id'] ?>"><u><i class="fa fa-trash"></i> Hapus Data</u></a>
              </td>
						</tr>
					<?php
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
		  </div>
			<!--form data--->
	</div>
</div><!--page content-->
<!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
<?php
include "site_footer.php";
?>
