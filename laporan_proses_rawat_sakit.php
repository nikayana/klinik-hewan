<?php 
	include "koneksi.php";
	 
?>
<?php 
$nama_dokumen='PDF With MPDF'; //Beri nama file PDF hasil.
define('_MPDF_PATH','MPDF/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4-L');
$w = 24*10;
$h = 14*10;
$mpdf=new mPDF('utf-8',array($w,$h)); // Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body onload="">
<style>
	.clearfix{
		clear:both;
	}
</style>
<div id="header" style=" font-size:10px;">
	<h2 style="text-align: center;">Laporan Berobat Rawat Sakit</h2>
	<div style="width:20%; height:60px; float:left;">
    	<img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/><br /><br /> 
    </div>
    <div class="clearfix"></div>
    <div style="width:80%; height:auto; float:left;"> 
        <p style="margin-top:-10px;">Jl. LC Intan II / Gang X No.1, Denpasar Utara | 081 916 159 276 </p>
    </div> 
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<hr />
<div id="conten" style="margin-top:50px;"> 
	<table class="table" style="width: 90%; margin-top:-60px; font-size:10px;" border="1">
        <thead>
			<tr>
				<th>No</th>
				<th>Nota</th>
				<th>Nama Pemilik</th>
				<th>Alamat</th> 
				<th>Nama Pasien</th> 
				<th>Tgl Berobat</th>  

				<th>Berat</th>  
				<th>Temp</th>  
				<th>Crt</th>  
				<th>Status Vaksin</th>
				<th>Keterangan</th>
				<th>Obat</th>
				<th>Tgl Titip</th>
				<th>Tgl Ambil</th>
				<th>Biaya</th>   
			</tr> 
        </thead>
        </tbody>
			<?php 
			$n= 1;
			$tgl_awal = date('Y-m-d', strtotime($_GET['tanggal_awal']));
			$tgl_awal = $tgl_awal.' 00:01:01';

			$tgl_akhir = date('Y-m-d',strtotime($_GET['tanggal_akhir']));
			$tgl_akhir = $tgl_akhir.' 23:00:00';
			//get all berobat
			$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik inner join tb_histori_rawat_sakit as c on a.code_pemilik = c.code_pemilik
				where 
				(c.tgl_berobat BETWEEN '$tgl_awal' AND '$tgl_akhir')
				order by c.tgl_berobat desc");
			$total = 0;
			$pasien = 0;
			while($foo = mysql_fetch_array($res)){
				$obat = "";
				$dataobatquery = mysql_query("SELECT j.jasa_barang_name FROM rawat_jalan_obat AS r 
				LEFT JOIN tb_jasabarang AS j ON r.jasa_barang_id=j.jasa_barang_id 
				WHERE r.nota='".$foo['nota']."'");
				while($dataobat = mysql_fetch_array($dataobatquery)){
					if(empty($obat)){
						$obat .=$dataobat['jasa_barang_name'];
					}else{
						$obat .=", ".$dataobat['jasa_barang_name'];
					}
				}
				if(empty($obat)){
					$obat = $foo['obat'];
				}
				if(empty($obat)){
					$obat = "-";
				}
				$total += $foo['a_biaya'];
				$pasien += 1;
			?>
				<tr>
					<td><?php echo $n++;?></td> 
					<td><?php echo $foo['nota'];?></td>
					<td><?php echo $foo['nama_pemilik'];?></td>
					<td><?php echo $foo['alamat_pemilik']; ?></td> 
					<td><?php echo $foo['nama_hewan'];?></td>
					<td><?php echo $foo['tgl_berobat']; ?></td> 

					<td><?php echo $foo['a_berat'].'Kg';?></td>
					<td><?php echo $foo['a_temp'];?></td>
					<td><?php echo $foo['a_crt']; ?></td> 
					<td><?php echo $foo['a_status_vaksin'];?></td>
					<td><?php echo $foo['a_keterangan']; ?></td>
					<td><?php echo $obat; ?></td>
					<td><?php echo $foo['tgl_titip']; ?></td>
					<td><?php echo $foo['tgl_ambil']; ?></td> 
					<td><?php echo "Rp.".number_format($foo['a_biaya']); ?></td> 
	 
				</tr>
			<?php 
				}
			?>
        </tbody>
    </table>
    <table cellspacing="0" style="width: 90%;   font-size:10px;" border="solid 1px #ddd">
    	<tr>
        	<td>Total Pasien</td>  
        	<td>Total Berobat</td>  
        </tr>
        <tr> 
             <td><?php echo number_format($pasien);?></td>
             <td><?php echo "Rp. ".number_format($total);?></td>
        </tr>
         
    </table>
    <hr />
    <p>Periode Laporan Berobat : <?php echo $_GET['tanggal_awal'].' s/d '.$_GET['tanggal_akhir'];?></p>
</div>
<div class="clearfix"></div>
<div id="footer">
	
</div>
</body>

</html>
<?php 

$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');


exit;
?>
<script>
		window.print();
		//window.close();
</script>