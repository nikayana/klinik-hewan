<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$msg_error = '';

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_jadwal"){
	$mode = $_GET['type'];
	$id = $_GET['jadwal'];
	$res = mysql_query("select * from tb_jadwal where id = $id ");
	$foo = mysql_fetch_array($res);
	$id = $foo['id'];
	$dokter = $foo['dokter'];
	$tgl_mulai = $foo['tgl_mulai'];
	$tgl_akhir = $foo['tgl_akhir'];
	$note = $foo['note'];
	$keterangan = $foo['keterangan']; 
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$dokter = $_POST['dokter'];
	$tgl_mulai = $_POST['tgl_mulai'];
	$tgl_akhir = $_POST['tgl_akhir'];
	$note = $_POST['note'];
	$keterangan = $_POST['keterangan'];
	
    $res = mysql_query("
			insert into tb_jadwal (dokter, tgl_mulai, tgl_akhir, note, keterangan) values ('$dokter','".$tgl_mulai."','".$tgl_akhir."','".$note."', '".$keterangan."')
		");
		 
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_jadwal.php');
		}else{
			$msg_error = "Gagal simpan.";
		}

}

//save edit data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	 
	$mode = 'edit';
	$id = $_POST['id'];
    $dokter = $_POST['dokter'];
	$tgl_mulai = $_POST['tgl_mulai'];
	$tgl_akhir = $_POST['tgl_akhir'];
	$note = $_POST['note'];
	$keterangan = $_POST['keterangan'];
	
	$res = mysql_query("select * from tb_jadwal where id = $id ");
	$foo = mysql_num_rows($res);
 
	if($foo > 0){
		$res = mysql_query("
			UPDATE tb_jadwal SET dokter = '$dokter', tgl_mulai = '$tgl_mulai' , tgl_akhir = '$tgl_akhir', note ='$note', keterangan='$keterangan' WHERE id = $id
		");
		echo "UPDATE tb_jadwal SET dokter = '$dokter', tgl_mulai = '$tgl_mulai' , tgl_akhir = '$tgl_akhir', note ='$note', keterangan='$keterangan' WHERE id = $id";
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_jadwal.php');
		}else{
			$msg_error = "Data gagal tersimpan.";
		}
	}
}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Jadwal</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_jadwal.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST"  id="demo-form2"  class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dokter<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					   <select name="dokter" class="form-control" required>
					       <option value="">pilih dokter</option>
					       <?php 
					        $res = mysql_query("select * from tb_dokter");
						    while($foo = mysql_fetch_array($res)){
						        if(isset($dokter) && $dokter ==  $foo['dokter_id']){
									$selected="selected='selected'";
								}else{
									$selected="";
								}
					       ?>
					            <option <?php echo $selected; ?> value="<?php echo $foo['dokter_id']; ?>"><?php echo $foo['dokter_name']; ?></option>
					       <?php } ?>
					   </select>

                      <input type="hidden" id="id"  class="form-control col-md-7 col-xs-12" name="id" value="<?php echo (!empty($id)) ? $id : '' ;?>">

                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tgl Mulai<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="datetime" id="tgl_mulai" required="required" class="form-control col-md-7 col-xs-12 datetimepicker" name="tgl_mulai" value="<?php echo (isset($tgl_mulai)) ? $tgl_mulai : '' ;?>">
					</div>
				  </div>
				  
				   <div class="form-group" style="display:none;">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tgl Selesai<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="datetime" id="tgl_akhir"  class="form-control col-md-7 col-xs-12 datetimepicker" name="tgl_akhir" value="<?php echo (isset($tgl_akhir)) ? $tgl_akhir : '' ;?>">
					</div>
				  </div>

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Note Singkat<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="note"  class="form-control col-md-7 col-xs-12" name="note" value="<?php echo (isset($note)) ? $note : '-' ;?>">
					</div>
				  </div>
				   
				    <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					    <textarea id="keterangan"   class="form-control col-md-7 col-xs-12" name="keterangan"><?php echo (isset($keterangan)) ? $keterangan : '-' ;?></textarea> 
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_jadwal.php"><button type="button" class="btn btn-primary">Cancel</button></a>
					  
					  <button type="submit" class="btn btn-success">Save</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>


<script type="text/javascript">
	$(document).ready(function(){
		$('.datapicker').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
	});
</script>

