<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$username = '';
$password = '';
$msg_error = '';
$perawatan = isset($_GET['perawatan']) ? $_GET['perawatan'] : "";
//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_pasien"){
	$mode = $_GET['type'];
	$id_pasien = $_GET['id_pasien'];
	$res = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik where b.id_pasien = '$id_pasien' ");
	$foo = mysql_fetch_array($res);
	$id_pemilik = $foo['id_pemilik'];
	$nama_pemilik = $foo['nama_pemilik'];
	$alamat_pemilik = $foo['alamat_pemilik'];
	$no_tlp = $foo['no_tlp'];
	$anamnesa = $foo['anamnesa'];
	$nama_hewan = $foo['nama_hewan'];
	$jenis = $foo['jenis'];
	$signalemen = $foo['signalemen'];
	$jenis_kelamin = $foo['jenis_kelamin'];
	$umur_hewan = $foo['umur_hewan'];
	$ras_hewan = $foo['ras_hewan'];
	$warna_hewan = $foo['warna_hewan'];
	$email = $foo['email'];
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$get_l = mysql_query("select max(id_counter_pasien) as las_id from tb_pasien");
	$foo_l = mysql_fetch_assoc($get_l);
	$id = $foo_l['las_id'] + 1;
	$pasien_id = 'P00'.$id;
	//insert pasein baru
	$nama_pemilik = $_POST['nama_pemilik'];
	$alamat_pemilik = $_POST['alamat_pemilik'];
	$no_tlp = $_POST['no_tlp'];
	$anamnesa = $_POST['anamnesa'];
	$email = $_POST['email'];
	$perawatan = $_POST['perawatan'];


	$res = mysql_query("
		insert into tb_pemilik
		(code_pemilik, nama_pemilik, alamat_pemilik, no_tlp, anamnesa, email)
		values
		('$pasien_id', '$nama_pemilik', '$alamat_pemilik', '$no_tlp', '$anamnesa', '$email')
		");

	$nama_hewan = $_POST['nama_hewan'];
	$jenis = $_POST['jenis'];
	$signalemen = $_POST['signalemen'];
	$umur_hewan = $_POST['umur_hewan'];
	$ras_hewan = $_POST['ras_hewan'];
	$warna_hewan = $_POST['warna_hewan'];
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$tgl_update = date('Y-m-d H:i:s');

	$res2 = mysql_query("
		insert into tb_pasien
		(code_pemilik, id_pasien, nama_hewan, jenis, signalemen, tgl_update, jenis_kelamin, umur_hewan, ras_hewan, warna_hewan)
		values
		('$pasien_id', '$pasien_id', '$nama_hewan', '$jenis', '$signalemen', '$tgl_update', '$jenis_kelamin', '$umur_hewan', '$ras_hewan', '$warna_hewan')
		");

	if($res && $res2){
		if($perawatan == "rawatsehat"){
			header('location: form_rawat_sehat.php?code_pemilik='.$pasien_id.'&type=new_rawat_sehat');
		}else if($perawatan == "rawatsakit"){
			header('location: form_rawat_sakit.php?code_pemilik='.$pasien_id.'&type=new_rawat_sakit');
		}else if($perawatan == "rawatinap"){
			header('location: form_rawat_inap.php?code_pemilik='.$pasien_id.'&type=new_rawat_inap');
		}else{
			header('location: data_pasien.php');
		}
		 
	}else{
		$msg_error .= 'Gagal simpan data pasien, ulangi input data dengan benar';
	}
}

//save edit data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	$id_pasien = $_POST['id_pasien'];
	$id_pemilik = $_POST['id_pemilik'];
	$nama_pemilik = $_POST['nama_pemilik'];
	$alamat_pemilik = $_POST['alamat_pemilik'];
	$no_tlp = $_POST['no_tlp'];
	$anamnesa = $_POST['anamnesa'];

	$nama_hewan = $_POST['nama_hewan'];
	$jenis = $_POST['jenis'];
	$signalemen = $_POST['signalemen'];
	$tgl_update = date('Y-m-d H:i:s');
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$umur_hewan = $_POST['umur_hewan'];
	$ras_hewan = $_POST['ras_hewan'];
	$warna_hewan = $_POST['warna_hewan'];
	$email = $_POST['email'];

	$res = mysql_query("UPDATE `tb_pemilik` SET `nama_pemilik` = '$nama_pemilik', `alamat_pemilik` = '$alamat_pemilik', `no_tlp` = '$no_tlp', `anamnesa` = '$anamnesa', `email` = '$email' WHERE `tb_pemilik`.`id_pemilik` = $id_pemilik;");

	$res2 = mysql_query("UPDATE `tb_pasien` SET `nama_hewan` = '$nama_hewan', `jenis` = '$jenis', `signalemen` = '$signalemen', `tgl_update` = '$tgl_update', `jenis_kelamin` = '$jenis_kelamin', `umur_hewan` = '$umur_hewan', `ras_hewan` = '$ras_hewan', `warna_hewan` = '$warna_hewan' WHERE `tb_pasien`.`id_pasien` = '$id_pasien';");

	header('location: data_pasien.php');
}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Pasien</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_pasien.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					<div class="row">
						<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
									  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
									</div>
								</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<h4><b>IDENTIFIKASI HEWAN / <i>ANIMAL IDENTIFICATION</i> </b></h4>
						  <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama / <i>Name</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <input type="text" id="nama_hewan" required="required" class="form-control col-md-7 col-xs-12" name="nama_hewan" value="<?php echo (isset($nama_hewan)) ? $nama_hewan : '' ;?>">
								</div>
						  </div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Kelamin / <i>Sex</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <select id="jenis_kelamin" required="required" class="form-control col-md-7 col-xs-12" name="jenis_kelamin">
										<option value="">Pilih Jenis Kelamin</option>
										<option value="0" <?php echo isset($jenis_kelamin) && $jenis_kelamin == "0" ? "selected='selected'" : "" ?>>Betina</option>
										<option value="1"<?php echo isset($jenis_kelamin) && $jenis_kelamin == "1" ? "selected='selected'" : "" ?>>Jantan</option>
									</select>
								</div>
						  </div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Umur / <i>Age</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <input type="text" id="umur_hewan" required="required" class="form-control col-md-7 col-xs-12" name="umur_hewan" value="<?php echo (isset($umur_hewan)) ? $umur_hewan : '' ;?>">
								</div>
						  </div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Spesies / <i>Species</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <input type="text" id="jenis" required="required" class="form-control col-md-7 col-xs-12" name="jenis" value="<?php echo (isset($jenis)) ? $jenis : '' ;?>">
								</div>
						  </div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Ras / <i>Breed</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <input type="text" id="ras" required="required" class="form-control col-md-7 col-xs-12" name="ras_hewan" value="<?php echo (isset($ras_hewan)) ? $ras_hewan : '' ;?>">
								</div>
						  </div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Warna / <i>Colour</i> <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <input type="text" id="warna" required="required" class="form-control col-md-7 col-xs-12" name="warna_hewan" value="<?php echo (isset($warna_hewan)) ? $warna_hewan : '' ;?>">
								</div>
						  </div>
					   <div class="form-group" style="display:none;">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Anamnesa<span class="required">*</span>
							 </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="anamnesa" required="required" class="form-control col-md-7 col-xs-12" name="anamnesa" value="<?php echo (isset($anamnesa)) ? $anamnesa : '-' ;?>">
							</div>
					  </div>

					   <div class="form-group" style="display:none;">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Signalemen<span class="required">*</span>
							 </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="signalemen" required="required" class="form-control col-md-7 col-xs-12" name="signalemen" value="<?php echo (isset($signalemen)) ? $signalemen : '-' ;?>">
							</div>
					  </div>
					</div>
					<div class="col-md-6">
						<h4><b>INFORMASI PEMILIK / <i>OWNER INFORMASI</i></b></h4>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama / <i>Name</i> <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" id="nama_pemilik" required="required" class="form-control col-md-7 col-xs-12" name="nama_pemilik" value="<?php echo (isset($nama_pemilik)) ? $nama_pemilik : '' ;?>">
								<input type="hidden" id="id_pasien"  class="form-control col-md-7 col-xs-12" name="id_pasien" value="<?php echo (isset($id_pasien)) ? $id_pasien : '' ;?>">
								<input type="hidden" id="id_pemilik"  class="form-control col-md-7 col-xs-12" name="id_pemilik" value="<?php echo (isset($id_pemilik)) ? $id_pemilik : '' ;?>">
								<input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat / <i>Address</i> <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="alamat_pemilik" required="required" class="form-control col-md-7 col-xs-12" name="alamat_pemilik" value="<?php echo (isset($alamat_pemilik)) ? $alamat_pemilik : '' ;?>">
							</div>
					  </div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Telepon / <i>Telephone</i> <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="no_tlp" required="required" class="form-control col-md-7 col-xs-12" name="no_tlp" value="<?php echo (isset($no_tlp)) ? $no_tlp : '' ;?>">
							</div>
					  	</div>
						
						  <div class="form-group" style="display:none;">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email / <i>Email</i> <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <input type="text" id="email" required="required" class="form-control col-md-7 col-xs-12" name="email" value="<?php echo (isset($email)) ? $email : '-' ;?>">
							</div>
					  </div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Perawatan</i> <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
							  <select name="perawatan" class="select2" style="width:100%" <?php if(isset($_GET['type']) && $_GET['type'] != 'edit_pasien'){echo 'required="required"';} ?>>
							  	<option value="">Pilih Jenis Perawatan</option>
								  <option value="rawatsehat" <?php echo isset($perawatan) && $perawatan == "rawatsehat" ? "selected='selected'" : "" ?>>Rawat Sehat</option>
								  <option value="rawatsakit" <?php echo isset($perawatan) && $perawatan == "rawatsakit" ? "selected='selected'" : "" ?>>Rawat Sakit</option>
								  <option value="rawatinap" <?php echo isset($perawatan) && $perawatan == "rawatinap" ? "selected='selected'" : "" ?>>Rawat Inap</option>
							  </select>
							</div>
					  	</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<center><a href="data_pasien.php"><button type="button" class="btn btn-primary">Cancel</button></a>
									<button type="submit" class="btn btn-success">Save</button>
								</center>
							</div>
						</div>
					</div>
				</div>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>
