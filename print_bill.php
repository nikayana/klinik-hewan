<?php
	include "koneksi.php";

?>
<?php
$nota = isset($_GET['nota']) ? $_GET['nota'] : "";
$datapasienquery = mysql_query("SELECT * FROM tb_pemilik AS tp
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik
LEFT JOIN tb_histori_rawat_sakit AS th ON tp.code_pemilik=th.code_pemilik
WHERE th.nota='".$nota."'");
$datapasien = mysql_fetch_array($datapasienquery);
$jenis = "-";
$dokter = "";
if(isset($datapasien['jenis_kelamin'])){
	if($datapasien['jenis_kelamin'] == '0'){
		$jenis = "Betina";
	}else if($datapasien['jenis_kelamin'] == '1'){
		$jenis = "Jantan";
	}
}
$datadokterquery = mysql_query("SELECT * FROM rawat_jalan_dokter AS rjd
LEFT JOIN tb_dokter AS td ON rjd.dokter_id=td.dokter_id
WHERE rjd.nota='".$nota."'");
while($datadokter = mysql_fetch_array($datadokterquery)){
	if(empty($dokter)){
		$dokter .=$datadokter['dokter_name'];
	}else{
		$dokter .=", ".$datadokter['dokter_name'];
	}
}
if(empty($dokter)){
	$dokter = "-";
}

$nama_dokumen='Nota Pembayaran Rawat Jalan.pdf'; //Beri nama file PDF hasil.
define('_MPDF_PATH','MPDF/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4');
$w = 24*10;
$h = 14*10;
$mpdf=new mPDF('utf-8',array($w,$h)); // Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body onload="">
<style>
	.clearfix{
		clear:both;
	}
	td {
		padding:2px;
	}
	.td{
		padding:2px;
		border:1px solid #000;
	}
</style>
<div id="header" style=" font-size:10px;">
		<div style="width:100%;padding:5px;">
			<p align="center">
    		    <img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/><br> 
			</p>
			<p align="center" style="margin-top:-5px;">Jl. LC Intan II / Gang X No.1, Denpasar Utara | Telp: 081 916 159 276 </p>
			<hr>
    </div>
</div>
<div id="conten">
	<table cellpadding=0 cellspacing=0 border=0 style="width:100%;font-size:10px;">
		<tbody>
				<tr>
					<td width="15%">No</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nota']) ? $datapasien['nota'] : "" ?><td>
					<td width="15%">Nama Hewan</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_hewan']) ? $datapasien['nama_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Nama</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_pemilik']) ? $datapasien['nama_pemilik'] : "" ?><td>
					<td width="15%">Spesies</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['jenis']) ? $datapasien['jenis'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Telepon</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['no_tlp']) ? $datapasien['no_tlp'] : "" ?><td>
					<td width="15%">Ras</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['ras_hewan']) ? $datapasien['ras_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Email</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['email']) ? $datapasien['email'] : "" ?><td>
					<td width="15%">Jenis</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo $jenis ?><td>
				</tr>
				<tr>
					<td width="15%">Tanggal Berobat</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['tgl_titip']) ? date("d F Y H:i:s", strtotime($datapasien['tgl_titip'])) : "" ?><td>
					<td width="15%">Dokter</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo $dokter ?><td>
				</tr>
		</tbody>
	</table>
	<table cellspacing=0 cellpadding=0 class="table" style="width: 100%; font-size:10px;" border="0">
        <thead>
			<tr>
				<th class="td">No</th>
				<th class="td">Jasa / Barang</th>
				<th class="td">Harga</th>
				<th class="td">Jumlah</th>
				<th class="td">Subtotal</th>
			</tr>
        </thead>
        </tbody>
			<?php
			$n= 1;

			//get all berobat
			$res = mysql_query("select rjo.*, tj.jasa_barang_name from rawat_jalan_obat as rjo
			LEFT JOIN tb_jasabarang AS tj ON rjo.jasa_barang_id=tj.jasa_barang_id
			WHERE rjo.nota='".$nota."'");
			$total = 0;
			while($foo = mysql_fetch_array($res)){
				$total += ($foo['jasa_barang_price']*$foo['jasa_barang_qty']);
			?>
				<tr>
					<td class="td"><?php echo $n++;?></td>
					<td class="td"><?php echo $foo['jasa_barang_name'];?></td>
					<td class="td" align="right"><?php echo "Rp.".number_format($foo['jasa_barang_price']);?></td>
					<td class="td" align="right"><?php echo $foo['jasa_barang_qty']; ?></td>
					<td class="td" align="right"><?php echo "Rp.".number_format($foo['jasa_barang_price']*$foo['jasa_barang_qty']); ?></td>

				</tr>
			<?php
				}
			?>
				<tr>
					<td class="td" colspan="4" align="right"><b>TOTAL</b></td>
					<td class="td" align="right"><b><?php echo "Rp.".number_format($total); ?></b></td>
				</tr>
				<tr>
					<td class="td" colspan="4" align="right"><b>Bayar</b></td>
					<td class="td" align="right"><b><?php echo "Rp.".number_format(isset($datapasien['bayar']) ? $datapasien['bayar'] : 0); ?></b></td>
				</tr>
				<tr>
					<td class="td" colspan="4" align="right"><b>Sisa</b></td>
					<td class="td" align="right"><b><?php echo "Rp.".number_format($total-(isset($datapasien['bayar']) ? $datapasien['bayar'] : 0)); ?></b></td>
				</tr>
        </tbody>
    </table>
    <p>Tanggal Cetak : <?php echo date('d-m-Y H:i:s'); ?></p>
    <hr />
</div>
<div class="clearfix"></div>
<div id="footer">

</div>
</body>

</html>
<?php

$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');


exit;
?>
<!--script>
		window.print();
		//window.close();
</script-->
