<?php 
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$username = '';
$password = '';
$msg_error = '';

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_user"){
	$mode = $_GET['type'];
	$id_user = $_GET['id_user'];
	$res = mysql_query("select * from tb_user where id_user = $id_user ");
	$foo = mysql_fetch_array($res);
	$id_user = $foo['id_user'];
	$nama_user = $foo['nama_user'];
	$username = $foo['username'];
	$password = $foo['password'];
	 
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$nama_user = $_POST['nama_user'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$hak_akses = $_POST['hak_akses']; 
	$res = mysql_query("select * from tb_user where nama_user = '$nama_user' AND username = '$username' AND hak_akses = '$hak_akses' ");
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$msg_error = "Gagal simpan, User ini sudah ada";
	}else{
		$res = mysql_query("
			insert into tb_user (nama_user, password, username, hak_akses) values ('$nama_user','$password','$username','$hak_akses')
		");
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_staff.php');
		}else{
			$msg_error = "Gagal update.";
		}
	}
	
}

//save edit data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	$id_user = $_POST['id_user'];
	$mode = 'edit';
	$nama_user = $_POST['nama_user'];
	$username = $_POST['username'];
	$password = $_POST['password_baru'];
	$hak_akses = $_POST['hak_akses'];
	$res = mysql_query("select * from tb_user where id_user = $id_user ");
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$res = mysql_query("
			UPDATE tb_user SET nama_user = '$nama_user', password = '$password' , username = '$username', hak_akses ='$hak_akses' WHERE id_user = $id_user
		");
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_staff.php');
		}else{
			$msg_error = "Data gagal tersimpan.";
		}
	}
}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">
		  
		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form User</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_staff.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama User<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="nama_user" required="required" class="form-control col-md-7 col-xs-12" name="nama_user" value="<?php echo (isset($nama_user)) ? $nama_user : '' ;?>">
                      
                      <input type="hidden" id="id_user"  class="form-control col-md-7 col-xs-12" name="id_user" value="<?php echo (isset($id_user)) ? $id_user : '' ;?>">
                      
                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="username" required="required" class="form-control col-md-7 col-xs-12" name="username" value="<?php echo (isset($username)) ? $username : '' ;?>">
					</div>
				  </div>
				 
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <?php if(isset($password) && $password !=''){ ?>Lama <?php }?><span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="password" id="password" required="required" class="form-control col-md-7 col-xs-12" name="password" value="<?php echo (isset($password)) ? $password : '' ;?>">
					</div>
				  </div>
				  <?php if(isset($password) && $password !=''){ ?>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password Baru<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="password" id="password" required="required" class="form-control col-md-7 col-xs-12" name="password_baru" value="<?php echo (isset($password)) ? $password : '' ;?>">
					</div>
				  </div>
				  <?php } ?>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hak Akses<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <select name="hak_akses" class="form-control col-md-7 col-xs-12">
					  	<option value="Administrator">Administrator</option> 
					  	<option value="Dokter">Dokter</option>
					  	<option value="Staff">Staff</option>
					  </select>

					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_staff.php"><button type="button" class="btn btn-primary">Cancel</button></a>
					  <button type="submit" class="btn btn-success">Save</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php 
include "site_footer.php";
?>