<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$dokter_name = '';
$dokter_address = '';
$dokter_phone = '';
$dokter_email = '';
$dokter_jk = 0;
$id_dokter = 0;
$msg_error = '';

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_dokter"){
	$mode = $_GET['type'];
	$id_dokter = $_GET['dokter_id'];
	$res = mysql_query("select * from tb_dokter where dokter_id = $id_dokter ");
	$foo = mysql_fetch_array($res);
	$id_dokter = $foo['dokter_id'];
	$dokter_name = $foo['dokter_name'];
	$dokter_address = $foo['dokter_address'];
	$dokter_phone = $foo['dokter_phone'];
	$dokter_email = $foo['dokter_email'];
	$dokter_jk = $foo['dokter_jk'];

}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$dokter_name = $_POST['dokter_name'];
	$dokter_address = $_POST['dokter_address'];
	$dokter_phone = $_POST['dokter_phone'];
	$dokter_email = $_POST['dokter_email'];
	$dokter_jk = $_POST['dokter_jk'];
	$res = mysql_query("select * from tb_dokter where dokter_email = '$dokter_email' ");
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$msg_error = "Gagal simpan, Data dokter ini sudah ada";
	}else{
		$res = mysql_query("
			insert into tb_dokter (dokter_name, dokter_address, dokter_phone, dokter_email, dokter_jk, dokter_datecreated, dokter_datemodified) values ('$dokter_name','$dokter_address','$dokter_phone','$dokter_email', '$dokter_jk', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')
		");
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_dokter.php');
		}else{
			$msg_error = "Gagal update.";
		}
	}

}

//save edit data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	$id_dokter = $_POST['dokter_id'];
	$mode = 'edit';
	$dokter_name = $_POST['dokter_name'];
	$dokter_address = $_POST['dokter_address'];
	$dokter_phone = $_POST['dokter_phone'];
	$dokter_email = $_POST['dokter_email'];
	$dokter_jk = $_POST['dokter_jk'];
	$res = mysql_query("select * from tb_dokter where dokter_id = $id_dokter ");
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$res = mysql_query("
			UPDATE tb_dokter SET dokter_name = '$dokter_name', dokter_address = '$dokter_address' , dokter_phone = '$dokter_phone', dokter_email ='$dokter_email', dokter_datemodified='".date("Y-m-d H:i:s")."' WHERE dokter_id = $id_dokter
		");
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_dokter.php');
		}else{
			$msg_error = "Data gagal tersimpan.";
		}
	}
}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Dokter</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_staff.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Dokter<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="dokter_name" required="required" class="form-control col-md-7 col-xs-12" name="dokter_name" value="<?php echo (isset($dokter_name)) ? $dokter_name : '' ;?>">

                      <input type="hidden" id="id_dokter"  class="form-control col-md-7 col-xs-12" name="dokter_id" value="<?php echo (isset($id_dokter)) ? $id_dokter : '' ;?>">

                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="dokter_address" required="required" class="form-control col-md-7 col-xs-12" name="dokter_address" value="<?php echo (isset($dokter_address)) ? $dokter_address : '' ;?>">
					</div>
				  </div>

				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Telepon<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="dokter_phone" required="required" class="form-control col-md-7 col-xs-12" name="dokter_phone" value="<?php echo (isset($dokter_phone)) ? $dokter_phone : '' ;?>">
					</div>
				  </div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="dokter_email" required="required" class="form-control col-md-7 col-xs-12" name="dokter_email" value="<?php echo (isset($dokter_email)) ? $dokter_email : '' ;?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kelamin<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <select name="dokter_jk" class="form-control col-md-7 col-xs-12">
							<?php
								$selected0='';
								$selected1='';

								if(isset($dokter_jk) && $dokter_jk == 0){
									$selected0="selected='selected'";
								}else{
									$selected1="selected='selected'";
								}
							?>
					  	<option value="0" <?php echo $selected0 ?>>Perempuan</option>
					  	<option value="1" <?php echo $selected1 ?>>Laki-laki</option>
					  </select>

					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_dokter.php"><button type="button" class="btn btn-primary">Cancel</button></a>
					  <button type="submit" class="btn btn-success">Save</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>
