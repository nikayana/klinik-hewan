<?php
session_start();
include "koneksi.php";


include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">

			<!---form data---->
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2> Data Pembayaran</h2>
				<ul class="nav navbar-right panel_toolbox">
				 <li><a href="form_pembayaran.php"><input type="button" class="btn btn-primary" value="Pembayaran Baru"></a></li>
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>

				  <li><a class="close-link" href="index.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">

				<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				  <thead>
					<tr>
					  <th>No</th>
					  <th>Status</th>
            <th>Tgl Pembayaran</th>
            <th>Nota Pembayaran</th>
            <th>Pasien</th>
					  <th>Nama Pemilik</th>
					  <th>Total Tagihan</th>
            <th>Total Diskon</th>
            <th>Total Deposit</th>
            <th>Total Pembayaran</th>
            <th>Total Kembalian</th>
					  <th><i class="fa fa-cogs"></i></th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$n= 1;
						//get all users
            $res = mysql_query("SELECT * FROM tb_pembayaran AS tp 
            LEFT JOIN tb_pasien AS ts ON tp.pasien_id=ts.id_counter_pasien 
            LEFT JOIN tb_pemilik AS tm ON ts.code_pemilik=tm.code_pemilik 
            ORDER BY pembayaran_date DESC");
						while($foo = mysql_fetch_array($res)){
					?>
						<tr>
						 <td><?php echo $n++;?></td>
						 <td>
						     <?php 
						        $sisa = $foo['pembayaran_billing'] - $foo['pembayaran_diskon'] - $foo['pembayaran_deposit'] - $foo['pembayaran_pembayaran'];
						        
						        if($sisa <= 0){
						            echo '<p style="background:green;">Lunas</p>';
						        }else{
						            echo '<p style="color:#fff;background:red;">Bon ('.number_format($sisa).')</p>';
						        }
						     ?>
						 </td>
						 <td><?php echo date("d F Y H:i:s", strtotime($foo['pembayaran_date'])) ?></td>
              <td><?php echo $foo['nota_pembayaran'];?></td>
              <td><?php echo $foo['nama_hewan'];?></td>
						  <td><?php echo $foo['nama_pemilik'];?></td>
						  <td><?php echo "Rp ".number_format($foo['pembayaran_billing']) ?></td>
						  <td><?php echo "Rp ".number_format($foo['pembayaran_diskon']) ?></td>
						  <td><?php echo "Rp ".number_format($foo['pembayaran_deposit']) ?></td>
						  <td><?php echo "Rp ".number_format($foo['pembayaran_pembayaran']) ?></td>
						  <td><?php echo "Rp ".number_format($foo['pembayaran_kembalian']) ?></td>
						  
              <td>
                <a href="form_pembayaran.php?nota=<?php echo $foo['pembayaran_id']; ?>&type=edit_pembayaran"><u><i class="fa fa-pencil"></i> Edit Data</u></a><br>
                <a href="print_pembayaran.php?code_pemilik=<?php echo $foo['pembayaran_id']; ?>"><u><i class="fa fa-print"></i> Cetak Nota</u></a><br>
                <a href="form_pembayaran.php?nota=<?php echo $foo['pembayaran_id']; ?>&type=hapus_pembayaran"><u><i class="fa fa-trash"></i> Hapus Data</u></a><br>
              </td>
						</tr>
					<?php
						}
					?>
				  </tbody>
				</table>

			  </div>
			</div>
		  </div>
			<!--form data--->
	</div>
</div><!--page content-->
<!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
<?php
include "site_footer.php";
?>
