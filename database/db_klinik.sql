-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2021 at 01:24 PM
-- Server version: 5.7.31-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_klinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `rawat_jalan_dokter`
--

CREATE TABLE `rawat_jalan_dokter` (
  `rawat_jalan_id` int(11) NOT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  `rawat_jalan_datecreated` datetime DEFAULT NULL,
  `rawat_jalan_datemodified` datetime DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rawat_jalan_obat`
--

CREATE TABLE `rawat_jalan_obat` (
  `pemeriksaan_obat_id` int(11) NOT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_price` double DEFAULT NULL,
  `rawat_jalan_datecreated` datetime DEFAULT NULL,
  `rawat_jalan_datemodified` datetime DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rawat_sehat_dokter`
--

CREATE TABLE `rawat_sehat_dokter` (
  `rawat_jalan_id` int(11) NOT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  `rawat_jalan_datecreated` datetime DEFAULT NULL,
  `rawat_jalan_datemodified` datetime DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rawat_sehat_obat`
--

CREATE TABLE `rawat_sehat_obat` (
  `pemeriksaan_obat_id` int(11) NOT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_price` double DEFAULT NULL,
  `rawat_jalan_datecreated` datetime DEFAULT NULL,
  `rawat_jalan_datemodified` datetime DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_dokter`
--

CREATE TABLE `tb_dokter` (
  `dokter_id` int(11) NOT NULL,
  `dokter_name` varchar(255) NOT NULL,
  `dokter_address` varchar(255) NOT NULL,
  `dokter_phone` varchar(255) NOT NULL,
  `dokter_email` varchar(255) NOT NULL,
  `dokter_jk` char(1) NOT NULL,
  `dokter_datecreated` datetime NOT NULL,
  `dokter_datemodified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori_berobat`
--

CREATE TABLE `tb_histori_berobat` (
  `id_histori` int(11) NOT NULL,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` float NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori_grooming`
--

CREATE TABLE `tb_histori_grooming` (
  `id_histori` int(11) NOT NULL,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori_rawat_inap`
--

CREATE TABLE `tb_histori_rawat_inap` (
  `id_histori` int(11) NOT NULL,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `obat` text NOT NULL,
  `bayar` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori_rawat_sakit`
--

CREATE TABLE `tb_histori_rawat_sakit` (
  `id_histori` int(11) NOT NULL,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `obat` text NOT NULL,
  `bayar` float DEFAULT NULL,
  `biaya` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_histori_rawat_sehat`
--

CREATE TABLE `tb_histori_rawat_sehat` (
  `id_histori` int(11) NOT NULL,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `dp_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `bayar` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal`
--

CREATE TABLE `tb_jadwal` (
  `id` int(11) NOT NULL,
  `dokter` int(11) NOT NULL,
  `tgl_mulai` datetime DEFAULT NULL,
  `tgl_akhir` datetime DEFAULT NULL,
  `keterangan` longtext,
  `note` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jasabarang`
--

CREATE TABLE `tb_jasabarang` (
  `jasa_barang_id` int(11) NOT NULL,
  `jasa_barang_name` varchar(255) DEFAULT NULL,
  `jasa_barang_price` double DEFAULT NULL,
  `jasa_barang_datecreated` datetime DEFAULT NULL,
  `jasa_barang_datemodified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pasien`
--

CREATE TABLE `tb_pasien` (
  `id_pasien` varchar(50) NOT NULL,
  `id_counter_pasien` int(11) NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `nama_hewan` varchar(200) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `signalemen` text NOT NULL,
  `tgl_update` datetime NOT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `umur_hewan` varchar(255) DEFAULT NULL,
  `ras_hewan` varchar(255) DEFAULT NULL,
  `warna_hewan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `pembayaran_id` int(11) NOT NULL,
  `pembayaran_diskon` float DEFAULT NULL,
  `pembayaran_deposit` float DEFAULT NULL,
  `pembayaran_kembalian` float DEFAULT NULL,
  `pembayaran_date` datetime DEFAULT NULL,
  `pembayaran_datecreated` datetime DEFAULT NULL,
  `pembayaran_datemodified` datetime DEFAULT NULL,
  `pembayaran_pembayaran` float DEFAULT NULL,
  `pasien_id` int(11) DEFAULT NULL,
  `nota_pembayaran` varchar(255) DEFAULT NULL,
  `pembayaran_billing` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran_inap`
--

CREATE TABLE `tb_pembayaran_inap` (
  `detail_id` int(11) NOT NULL,
  `pembayaran_id` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `pembayaran_tagihan` float DEFAULT NULL,
  `detail_datecreated` datetime DEFAULT NULL,
  `detail_datemodified` datetime DEFAULT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_price` float DEFAULT NULL,
  `jasa_barang_name` varchar(255) DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `jasa_barang_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran_sakit`
--

CREATE TABLE `tb_pembayaran_sakit` (
  `detail_id` int(11) NOT NULL,
  `pembayaran_id` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `pembayaran_tagihan` float DEFAULT NULL,
  `detail_datecreated` datetime DEFAULT NULL,
  `detail_datemodified` datetime DEFAULT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_name` varchar(255) DEFAULT NULL,
  `jasa_barang_price` float DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `jasa_barang_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran_sehat`
--

CREATE TABLE `tb_pembayaran_sehat` (
  `detail_id` int(11) NOT NULL,
  `pembayaran_id` int(11) DEFAULT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `pembayaran_tagihan` float DEFAULT NULL,
  `detail_datecreated` datetime DEFAULT NULL,
  `detail_datemodified` datetime DEFAULT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_name` varchar(255) DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `jasa_barang_date` date DEFAULT NULL,
  `jasa_barang_price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemeriksaan`
--

CREATE TABLE `tb_pemeriksaan` (
  `pemeriksaan_id` int(11) NOT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `pemeriksaan_date` datetime DEFAULT NULL,
  `pemeriksaan_jenis` text,
  `pemeriksaan_note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemeriksaan_dokter`
--

CREATE TABLE `tb_pemeriksaan_dokter` (
  `pemeriksaan_dokter_id` int(11) NOT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  `pemeriksaan_datecreated` datetime DEFAULT NULL,
  `pemeriksaan_datemodified` datetime DEFAULT NULL,
  `pemeriksaan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemeriksaan_obat`
--

CREATE TABLE `tb_pemeriksaan_obat` (
  `rawat_jalan_id` int(11) NOT NULL,
  `jasa_barang_id` int(11) DEFAULT NULL,
  `jasa_barang_price` double DEFAULT NULL,
  `pemeriksaan_datecreated` datetime DEFAULT NULL,
  `pemeriksaan_datemodified` datetime DEFAULT NULL,
  `jasa_barang_qty` int(11) DEFAULT NULL,
  `pemeriksaan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemilik`
--

CREATE TABLE `tb_pemilik` (
  `id_pemilik` int(11) NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `nama_pemilik` varchar(200) NOT NULL,
  `alamat_pemilik` varchar(200) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `anamnesa` varchar(200) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(200) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(11) NOT NULL,
  `hak_akses` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `username`, `password`, `hak_akses`) VALUES
(1, 'administrator', 'admin', 'admin@1234', 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rawat_jalan_dokter`
--
ALTER TABLE `rawat_jalan_dokter`
  ADD PRIMARY KEY (`rawat_jalan_id`);

--
-- Indexes for table `rawat_jalan_obat`
--
ALTER TABLE `rawat_jalan_obat`
  ADD PRIMARY KEY (`pemeriksaan_obat_id`);

--
-- Indexes for table `rawat_sehat_dokter`
--
ALTER TABLE `rawat_sehat_dokter`
  ADD PRIMARY KEY (`rawat_jalan_id`);

--
-- Indexes for table `rawat_sehat_obat`
--
ALTER TABLE `rawat_sehat_obat`
  ADD PRIMARY KEY (`pemeriksaan_obat_id`);

--
-- Indexes for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  ADD PRIMARY KEY (`dokter_id`);

--
-- Indexes for table `tb_histori_berobat`
--
ALTER TABLE `tb_histori_berobat`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `tb_histori_grooming`
--
ALTER TABLE `tb_histori_grooming`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `tb_histori_rawat_inap`
--
ALTER TABLE `tb_histori_rawat_inap`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `tb_histori_rawat_sakit`
--
ALTER TABLE `tb_histori_rawat_sakit`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `tb_histori_rawat_sehat`
--
ALTER TABLE `tb_histori_rawat_sehat`
  ADD PRIMARY KEY (`id_histori`);

--
-- Indexes for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jasabarang`
--
ALTER TABLE `tb_jasabarang`
  ADD PRIMARY KEY (`jasa_barang_id`);

--
-- Indexes for table `tb_pasien`
--
ALTER TABLE `tb_pasien`
  ADD PRIMARY KEY (`id_counter_pasien`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`pembayaran_id`);

--
-- Indexes for table `tb_pembayaran_inap`
--
ALTER TABLE `tb_pembayaran_inap`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `tb_pembayaran_sakit`
--
ALTER TABLE `tb_pembayaran_sakit`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `tb_pembayaran_sehat`
--
ALTER TABLE `tb_pembayaran_sehat`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `tb_pemeriksaan`
--
ALTER TABLE `tb_pemeriksaan`
  ADD PRIMARY KEY (`pemeriksaan_id`);

--
-- Indexes for table `tb_pemeriksaan_dokter`
--
ALTER TABLE `tb_pemeriksaan_dokter`
  ADD PRIMARY KEY (`pemeriksaan_dokter_id`);

--
-- Indexes for table `tb_pemeriksaan_obat`
--
ALTER TABLE `tb_pemeriksaan_obat`
  ADD PRIMARY KEY (`rawat_jalan_id`);

--
-- Indexes for table `tb_pemilik`
--
ALTER TABLE `tb_pemilik`
  ADD PRIMARY KEY (`id_pemilik`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rawat_jalan_dokter`
--
ALTER TABLE `rawat_jalan_dokter`
  MODIFY `rawat_jalan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rawat_jalan_obat`
--
ALTER TABLE `rawat_jalan_obat`
  MODIFY `pemeriksaan_obat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rawat_sehat_dokter`
--
ALTER TABLE `rawat_sehat_dokter`
  MODIFY `rawat_jalan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rawat_sehat_obat`
--
ALTER TABLE `rawat_sehat_obat`
  MODIFY `pemeriksaan_obat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  MODIFY `dokter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_histori_berobat`
--
ALTER TABLE `tb_histori_berobat`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_histori_grooming`
--
ALTER TABLE `tb_histori_grooming`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_histori_rawat_inap`
--
ALTER TABLE `tb_histori_rawat_inap`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_histori_rawat_sakit`
--
ALTER TABLE `tb_histori_rawat_sakit`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_histori_rawat_sehat`
--
ALTER TABLE `tb_histori_rawat_sehat`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_jasabarang`
--
ALTER TABLE `tb_jasabarang`
  MODIFY `jasa_barang_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pasien`
--
ALTER TABLE `tb_pasien`
  MODIFY `id_counter_pasien` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `pembayaran_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pembayaran_inap`
--
ALTER TABLE `tb_pembayaran_inap`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pembayaran_sakit`
--
ALTER TABLE `tb_pembayaran_sakit`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pembayaran_sehat`
--
ALTER TABLE `tb_pembayaran_sehat`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pemeriksaan`
--
ALTER TABLE `tb_pemeriksaan`
  MODIFY `pemeriksaan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pemeriksaan_dokter`
--
ALTER TABLE `tb_pemeriksaan_dokter`
  MODIFY `pemeriksaan_dokter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pemeriksaan_obat`
--
ALTER TABLE `tb_pemeriksaan_obat`
  MODIFY `rawat_jalan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pemilik`
--
ALTER TABLE `tb_pemilik`
  MODIFY `id_pemilik` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
