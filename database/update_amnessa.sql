/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.38-MariaDB : Database - db_rekam_hewan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_rekam_hewan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_rekam_hewan`;

/*Table structure for table `tb_histori_rawat_inap` */

DROP TABLE IF EXISTS `tb_histori_rawat_inap`;

CREATE TABLE `tb_histori_rawat_inap` (
  `id_histori` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `obat` text NOT NULL,
  `bayar` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0',
  `amnessa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_histori`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_histori_rawat_sakit` */

DROP TABLE IF EXISTS `tb_histori_rawat_sakit`;

CREATE TABLE `tb_histori_rawat_sakit` (
  `id_histori` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `obat` text NOT NULL,
  `bayar` float DEFAULT NULL,
  `biaya` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0',
  `amnessa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_histori`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_histori_rawat_sehat` */

DROP TABLE IF EXISTS `tb_histori_rawat_sehat`;

CREATE TABLE `tb_histori_rawat_sehat` (
  `id_histori` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(50) NOT NULL,
  `tgl_berobat` datetime NOT NULL,
  `code_pemilik` varchar(50) NOT NULL,
  `id_pasien` varchar(50) NOT NULL,
  `a_berat` int(11) NOT NULL,
  `a_temp` float NOT NULL,
  `a_crt` varchar(100) NOT NULL,
  `a_status_vaksin` varchar(100) NOT NULL,
  `a_keterangan` text NOT NULL,
  `a_biaya` int(11) NOT NULL,
  `dp_biaya` int(11) NOT NULL,
  `tgl_titip` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `bayar` float DEFAULT NULL,
  `payment_status` int(1) DEFAULT '0',
  `amnessa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_histori`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
