<?php
	include "koneksi.php";
 
?>
<?php
$nota = isset($_GET['code_pemilik']) ? $_GET['code_pemilik'] : "";
$datapasienquery = mysql_query("SELECT * FROM tb_pembayaran AS tp 
LEFT JOIN tb_pasien AS tps ON tp.pasien_id=tps.id_counter_pasien 
LEFT JOIN tb_pemilik AS tpm ON tps.code_pemilik=tpm.code_pemilik
WHERE tp.pembayaran_id='".$nota."'");
$datapasien = mysql_fetch_array($datapasienquery);
$jenis = "-";
$dokter = "";
if(isset($datapasien['jenis_kelamin'])){
	if($datapasien['jenis_kelamin'] == '0'){
		$jenis = "Betina";
	}else if($datapasien['jenis_kelamin'] == '1'){
		$jenis = "Jantan";
	}
}


$nama_dokumen='Nota Pembayaran.pdf'; //Beri nama file PDF hasil.
define('_MPDF_PATH','MPDF/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4');
$w = 14*10;
$h = 24*10;
$mpdf=new mPDF('utf-8',array($w,$h)); // Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body onload="">
<style>
	.clearfix{
		clear:both;
	}
	.td{
		padding:2px;
		border:1px solid #000;
	}
</style>
<div id="header" style=" font-size:10px;">
		<div style="width:100%;padding:5px;">
			<p align="center">
    		    <img src="images/logo1.jpeg"  width="40"/><img src="images/logo2.jpeg"  width="100"/><br> 
			</p>
			<p align="center" style="margin-top:-5px;">Jl. LC Intan II / Gang X No.1, Denpasar Utara | Telp: 081 916 159 276 </p>
			<hr>
    </div>
</div>
<div id="conten">
	<table cellpadding=0 cellspacing=0 border=0 style="width:100%;font-size:10px;">
		<tbody>
				<tr>
					<td width="15%">No</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nota_pembayaran']) ? $datapasien['nota_pembayaran'] : "" ?><td>
					<td width="15%">Nama Hewan</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_hewan']) ? $datapasien['nama_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Nama</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['nama_pemilik']) ? $datapasien['nama_pemilik'] : "" ?><td>
					<td width="15%">Spesies</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['jenis']) ? $datapasien['jenis'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Telepon</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['no_tlp']) ? $datapasien['no_tlp'] : "" ?><td>
					<td width="15%">Ras</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['ras_hewan']) ? $datapasien['ras_hewan'] : "" ?><td>
				</tr>
				<tr>
					<td width="15%">Email</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo isset($datapasien['email']) ? $datapasien['email'] : "" ?><td>
					<td width="15%">Jenis</td>
					<td width="1%">:</td>
					<td width="34%"><?php echo $jenis ?><td>
				</tr>
		</tbody>
	</table>
	<table cellspacing=0 cellpadding=0 class="table" style="width: 100%; font-size:8px;border:1px solid #000" border="0">
	<thead>
		<tr>
		<th class="td">No</th>
		<th class="td">Nota</th>
		<th class="td">Tgl Pemeriksaan</th>
		<th class="td">Jasa/Barang</th>
		<th class="td">Harga</th>
		<th class="td">Qty</th>
		<th class="td">Tagihan</th>
		</tr>
	</thead>
	<tbody id="tbrawatinap">
		<tr>
			<td class="td" colspan="7"><b>Rawat Sehat</b></td>
		</tr>
		<?php
			if(isset($datapasien['pembayaran_id']) && !empty($datapasien['pembayaran_id'])){
				$dataquery = mysql_query("SELECT * FROM tb_pembayaran_sehat WHERE pembayaran_id='".$datapasien['pembayaran_id']."'");
				$no = 1;
				while($rquery = mysql_fetch_array($dataquery)){
					?>
					<tr>
						<td class="td"> 
							<?php echo $no ?>
						</td>
						<td class="td"><?php echo $rquery['nota'] ?></td>
						<td class="td"><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
						<td class="td"><?php echo $rquery['jasa_barang_name'] ?></td>
						<td class="td" align="right"> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
						<td class="td" align="right"><?php echo number_format($rquery['jasa_barang_qty']) ?></td>
						<td class="td" align="right">Rp <?php echo number_format($rquery['jasa_barang_price']*$rquery['jasa_barang_qty']) ?></td>
					</tr>
					<?php
					$no++;
				}
			}
		?>
		<tr>
			<td class="td" colspan="7"><b>Rawat Sakit</b></td>
		</tr>
		<?php
			if(isset($datapasien['pembayaran_id']) && !empty($datapasien['pembayaran_id'])){
				$dataquery = mysql_query("SELECT * FROM tb_pembayaran_sakit WHERE pembayaran_id='".$datapasien['pembayaran_id']."'");
				$no = 1;
				while($rquery = mysql_fetch_array($dataquery)){
					?>
					<tr>
						<td class="td"> 
							<?php echo $no ?>
						</td>
						<td class="td"><?php echo $rquery['nota'] ?></td>
						<td class="td"><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
						<td class="td"><?php echo $rquery['jasa_barang_name'] ?></td>
						<td class="td" align="right"> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
						<td class="td" align="right"><?php echo number_format($rquery['jasa_barang_qty']) ?></td>
						<td class="td" align="right">Rp <?php echo number_format($rquery['jasa_barang_price']*$rquery['jasa_barang_qty']) ?></td>
					</tr>
					<?php
					$no++;
				}
			}
		?>
		<tr>
			<td class="td" colspan="7"><b>Rawat Inap</b></td>
		</tr>
		<?php
			if(isset($datapasien['pembayaran_id']) && !empty($datapasien['pembayaran_id'])){
				$dataquery = mysql_query("SELECT * FROM tb_pembayaran_inap WHERE pembayaran_id='".$datapasien['pembayaran_id']."'");
				$no = 1;
				while($rquery = mysql_fetch_array($dataquery)){
					?>
					<tr>
						<td class="td"> 
							<?php echo $no ?>
						</td>
						<td class="td"><?php echo $rquery['nota'] ?></td>
						<td class="td"><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
						<td class="td"><?php echo $rquery['jasa_barang_name'] ?></td>
						<td class="td" align="right"> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
						<td class="td" align="right"><?php echo number_format($rquery['jasa_barang_qty']) ?></td>
						<td class="td" align="right">Rp <?php echo number_format(($rquery['jasa_barang_price']*$rquery['jasa_barang_qty'])) ?></td>
					</tr>
					<?php
					$no++;
				}
			}
		?>
		<tr>
			<td class="td" align="right" colspan="6">Subtotal :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_billing'])?>/</td>
		</tr>
		<tr>
			<td class="td" align="right" colspan="6">Diskon :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_diskon'])?>/</td>
		</tr>
		<tr>
			<td class="td" align="right" colspan="6">Deposit :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_deposit'])?>/</td>
		</tr>
		<tr>
			<td class="td" align="right" colspan="6">Sisa Tagihan :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_billing']-$datapasien['pembayaran_diskon']-$datapasien['pembayaran_deposit'])?>/</td>
		</tr>
		<tr>
			<td class="td" align="right" colspan="6">Pembayaran :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_pembayaran'])?></td>
		</tr>
		<tr>
			<td class="td" align="right" colspan="6">Kembalian :</td>
			<td class="td" align="right"><?php echo "Rp ".number_format($datapasien['pembayaran_kembalian'])?></td>
		</tr>
		
	</tbody>
	</table>

    <hr />
</div>
<div class="clearfix"></div>
<div id="footer">

</div>
</body>

</html>
<?php

$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');


exit;

?>
<!--script>
		window.print();
		//window.close();
</script-->
