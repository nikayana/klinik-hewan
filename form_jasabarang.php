<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$jasabarang_name = '';
$jasabarang_price = '';
$jasabarang_id = 0;
$msg_error = '';

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_jasabarang"){
	$mode = $_GET['type'];
	$jasabarang_id = $_GET['jasa_barang_id'];
	$res = mysql_query("select * from tb_jasabarang where jasa_barang_id = $jasabarang_id ");
	$foo = mysql_fetch_array($res);
	$jasabarang_id = $foo['jasa_barang_id'];
	$jasabarang_name = $foo['jasa_barang_name'];
	$jasabarang_price = $foo['jasa_barang_price'];

}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$jasabarang_name = $_POST['jasa_barang_name'];
	$jasabarang_price = $_POST['jasa_barang_price'];
	$res = mysql_query("select * from tb_jasabarang where jasa_barang_name = '$jasabarang_name' ");
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$msg_error = "Gagal simpan, Data jasa & barang ini sudah ada";
	}else{
		$res = mysql_query("
			insert into tb_jasabarang (jasa_barang_name, jasa_barang_price, jasa_barang_datecreated, jasa_barang_datemodified) values ('$jasabarang_name','$jasabarang_price', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')
		");
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_jasabarang.php');
		}else{
			$msg_error = "Gagal update.";
		}
	}

}

//save edit data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	$jasabarang_id = $_POST['jasa_barang_id'];
	$mode = 'edit';
	$jasabarang_name = $_POST['jasa_barang_name'];
	$jasabarang_price = $_POST['jasa_barang_price'];
	 
	$res = mysql_query("select * from tb_jasabarang where jasa_barang_id = $jasabarang_id ");
	
	$foo = mysql_num_rows($res);
	if($foo > 0){
		$res = mysql_query("
			UPDATE tb_jasabarang SET jasa_barang_name = '$jasabarang_name', jasa_barang_price = '$jasabarang_price' ,  jasa_barang_datemodified='".date("Y-m-d H:i:s")."' WHERE jasa_barang_id = $jasabarang_id
		");
		 
		if($res){
			$msg_error = "Data berhasil tersimpan.";
			header('location: data_jasabarang.php');
		}else{
			$msg_error = "Data gagal tersimpan.";
		}
	}
}

include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Form Jasa & Barang</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_jasabarang.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Jasa & Barang<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="jasa_barang_name" required="required" class="form-control col-md-7 col-xs-12" name="jasa_barang_name" value="<?php echo (isset($jasabarang_name)) ? $jasabarang_name : '' ;?>">

                      <input type="hidden" id="jasa_barang_id"  class="form-control col-md-7 col-xs-12" name="jasa_barang_id" value="<?php echo (isset($jasabarang_id)) ? $jasabarang_id : '' ;?>">

                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == '' ) ? 'new' : 'edit'; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Harga<span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="jasa_barang_price" required="required" class="form-control col-md-7 col-xs-12" name="jasa_barang_price" value="<?php echo (isset($jasabarang_price)) ? $jasabarang_price : '' ;?>">
					</div>
				  </div>

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_jasabarang.php"><button type="button" class="btn btn-primary">Cancel</button></a>
					  <button type="submit" class="btn btn-success">Save</button>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>
