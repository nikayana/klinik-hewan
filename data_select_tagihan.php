<?php 
include "koneksi.php";
$pasien_id = isset($_POST['pasien_id']) ? $_POST['pasien_id'] : "";
$pasienexplode = explode(" | ", $pasien_id);
$pasien_id = isset($pasienexplode[0]) ? $pasienexplode[0] : 0 ;
$query = mysql_query("SELECT trs.*, rso.*, tj.jasa_barang_name FROM tb_histori_rawat_sehat AS trs 
LEFT JOIN tb_pemilik AS tp ON trs.code_pemilik=tp.code_pemilik 
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik 
LEFT JOIN rawat_sehat_obat AS rso ON trs.nota=rso.nota 
LEFT JOIN tb_jasabarang AS tj ON rso.jasa_barang_id=tj.jasa_barang_id 
WHERE tps.id_counter_pasien='".$pasien_id."' 
AND (trs.payment_status=0 OR trs.payment_status IS NULL)");
$json = array();
$total = 0;
$rawatsehat = "";
$no = 1;
while($r = mysql_fetch_array($query)){
    $rawatsehat .="
    <tr>
        <td> 
            $no
            <input type='hidden' name='rawatsehat_nota[]' value='".$r['nota']."'>
            <input type='hidden' name='rawatsehat_biaya[]' value='".$r['jasa_barang_price']."'>
            <input type='hidden' name='rawatsehat_jasabarangid[]' value='".$r['jasa_barang_id']."'>
            <input type='hidden' name='rawatsehat_jasabarangname[]' value='".$r['jasa_barang_name']."'>
            <input type='hidden' name='rawatsehat_jasabarangqty[]' value='".$r['jasa_barang_qty']."'>
            <input type='hidden' name='rawatsehat_jasabarangdate[]' value='".date("Y-m-d", strtotime($r['tgl_titip']))."'>
        </td>
        <td>".$r['nota']."</td>
        <td>".date("d F Y", strtotime($r['tgl_titip']))."</td>
        <td>".$r['jasa_barang_name']."</td>
        <td> Rp ".number_format($r['jasa_barang_price'])."</td>
        <td> Rp ".number_format($r['jasa_barang_qty'])."</td>
        <td>Rp ".number_format($r['jasa_barang_price']*$r['jasa_barang_qty'])."</td>
    </tr>";
    $total+=($r['jasa_barang_price']*$r['jasa_barang_qty']);
    $no++;
}
$query = mysql_query("SELECT trs.*, rso.*, tj.jasa_barang_name FROM tb_histori_rawat_sakit AS trs 
LEFT JOIN tb_pemilik AS tp ON trs.code_pemilik=tp.code_pemilik 
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik 
LEFT JOIN rawat_jalan_obat AS rso ON trs.nota=rso.nota 
LEFT JOIN tb_jasabarang AS tj ON rso.jasa_barang_id=tj.jasa_barang_id 
WHERE tps.id_counter_pasien='".$pasien_id."' 
AND (trs.payment_status=0 OR trs.payment_status IS NULL)");
$rawatsakit = "";

$no = 1;
while($r = mysql_fetch_array($query)){
    $rawatsakit .="
    <tr>
        <td> 
            $no
            <input type='hidden' name='rawatsakit_nota[]' value='".$r['nota']."'>
            <input type='hidden' name='rawatsakit_biaya[]' value='".$r['jasa_barang_price']."'>
            <input type='hidden' name='rawatsakit_jasabarangid[]' value='".$r['jasa_barang_id']."'>
            <input type='hidden' name='rawatsakit_jasabarangname[]' value='".$r['jasa_barang_name']."'>
            <input type='hidden' name='rawatsakit_jasabarangqty[]' value='".$r['jasa_barang_qty']."'>
            <input type='hidden' name='rawatsakit_jasabarangdate[]' value='".date("Y-m-d", strtotime($r['tgl_titip']))."'>
        </td>
        <td>".$r['nota']."</td>
        <td>".date("d F Y", strtotime($r['tgl_titip']))."</td>
        <td>".$r['jasa_barang_name']."</td>
        <td> Rp ".number_format($r['jasa_barang_price'])."</td>
        <td> Rp ".number_format($r['jasa_barang_qty'])."</td>
        <td>Rp ".number_format($r['jasa_barang_price']*$r['jasa_barang_qty'])."</td>
    </tr>";
    $total+=($r['jasa_barang_price']*$r['jasa_barang_qty']);
    $no++;
}

$query = mysql_query("SELECT trs.*, tpo.*,tps2.pemeriksaan_date, tj.jasa_barang_name FROM tb_histori_rawat_inap AS trs 
LEFT JOIN tb_pemilik AS tp ON trs.code_pemilik=tp.code_pemilik 
LEFT JOIN tb_pasien AS tps ON tp.code_pemilik=tps.code_pemilik 
LEFT JOIN tb_pemeriksaan AS tps2 ON trs.nota=tps2.nota 
LEFT JOIN tb_pemeriksaan_obat AS tpo ON tps2.pemeriksaan_id=tpo.pemeriksaan_id 
LEFT JOIN tb_jasabarang AS tj ON tpo.jasa_barang_id=tj.jasa_barang_id 
WHERE tps.id_counter_pasien='".$pasien_id."' 
AND (trs.payment_status=0 OR trs.payment_status IS NULL)");
$rawatinap = "";
$no = 1;
while($r = mysql_fetch_array($query)){
    $rawatinap .="
    <tr>
        <td> 
            $no
            <input type='hidden' name='rawatinap_nota[]' value='".$r['nota']."'>
            <input type='hidden' name='rawatinap_biaya[]' value='".$r['jasa_barang_price']."'>
            <input type='hidden' name='rawatinap_jasabarangid[]' value='".$r['jasa_barang_id']."'>
            <input type='hidden' name='rawatinap_jasabarangname[]' value='".$r['jasa_barang_name']."'>
            <input type='hidden' name='rawatinap_jasabarangqty[]' value='".$r['jasa_barang_qty']."'>
            <input type='hidden' name='rawatinap_jasabarangdate[]' value='".date("Y-m-d", strtotime($r['pemeriksaan_date']))."'>
        </td>
        <td>".$r['nota']."</td>
        <td>".date("d F Y", strtotime($r['pemeriksaan_date']))."</td>
        <td>".$r['jasa_barang_name']."</td>
        <td> Rp ".number_format($r['jasa_barang_price'])."</td>
        <td> Rp ".number_format($r['jasa_barang_qty'])."</td>
        <td>Rp ".number_format($r['jasa_barang_price']*$r['jasa_barang_qty'])."</td>
    </tr>";
    $total+=($r['jasa_barang_price']*$r['jasa_barang_qty']);
    $no++;
}

$json['rawatsehat'] = $rawatsehat;
$json['rawatsakit'] = $rawatsakit;
$json['rawatinap'] = $rawatinap;
$json['total'] = $total;
$json['pasien_id'] = $pasien_id;

echo json_encode($json);
?>