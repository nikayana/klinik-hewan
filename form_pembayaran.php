<?php
session_start();
//cek user login
if(!$_SESSION){header('location: login.php');}
include "koneksi.php";

$mode = '';
$username = '';
$password = '';
$msg_error = '';
$id = '';
$nota = isset($_GET['nota']) ? $_GET['nota'] : '';

$hidden = "display:none;";
$readonly = "";

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "hapus_pembayaran"){
	$mode = $_GET['type'];
	$nota = $_GET['nota'];
	$status = "";
	$query = mysql_query("SELECT * FROM tb_pembayaran WHERE pembayaran_id='".$nota."'");
	$foo = mysql_fetch_array($query);
	if(isset($foo['pembayaran_id'])){
		$delquery = mysql_query("DELETE FROM tb_pembayaran WHERE pembayaran_id='".$foo['pembayaran_id']."'");
		if($delquery){
			$status .=" GLOBAL OK ";
			//RAWAT SEHAT
			$error = 0;
			$querysehat = mysql_query("SELECT * FROM tb_pembayaran_sehat WHERE pembayaran_id='".$foo['pembayaran_id']."'");
			while($rsehat = mysql_fetch_array($querysehat)){
				$updatesehat = mysql_query("UPDATE tb_histori_rawat_sehat SET payment_status=0 WHERE nota='".$rsehat['nota']."'");
				$delsehat = mysql_query("DELETE FROM tb_pembayaran_sehat WHERE detail_id='".$rsehat['detail_id']."'");
				if(!$updatesehat){
					$status .=" | UPDATE SEHAT OK ";
					$error += 1;
				}

				if(!$delsehat){
					$status .=" | DELETE SEHAT OK ";
					$error+=1;
				}
			}

			//RAWAT SEHAT
			$querysakit = mysql_query("SELECT * FROM tb_pembayaran_sakit WHERE pembayaran_id='".$foo['pembayaran_id']."'");
			while($rsakit = mysql_fetch_array($querysakit)){
				$updatesakit = mysql_query("UPDATE tb_histori_rawat_sakit SET payment_status=0 WHERE nota='".$rsakit['nota']."'");
				$delsakit = mysql_query("DELETE FROM tb_pembayaran_sakit WHERE detail_id='".$rsakit['detail_id']."'");
				if(!$updatesakit){
					$status .=" | UPDATE SAKIT OK ";
					$error += 1;
				}

				if(!$delsakit){
					$status .=" | DELETE SAKIT OK ";
					$error += 1;
				}
			}

			//RAWAT inap
			$queryinap = mysql_query("SELECT * FROM tb_pembayaran_inap WHERE pembayaran_id='".$foo['pembayaran_id']."'");
			while($rinap = mysql_fetch_array($queryinap)){
				$updateinap = mysql_query("UPDATE tb_histori_rawat_inap SET payment_status=0 WHERE nota='".$rinap['nota']."'");
				$delinap = mysql_query("DELETE FROM tb_pembayaran_sakit WHERE detail_id='".$rinap['detail_id']."'");
				if(!$updateinap){
					$status .=" | UPDATE INAP OK ";
					$error += 1;
				}
				if(!$delinap){
					$status .=" | DELETE INAP OK ";
					$error += 1;
				}
			}
		}
		if($error == 0){
			header('location: data_pembayaran.php');
		}else{
			$msg_error .= 'Gagal hapus data berobat <br>';
		}

		
	}else{
		echo $status;
		$msg_error .= 'Gagal hapus data berobat <br>';

	}
}

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "new_pembayaran"){
	$mode = $_GET['type'];

}

//get data using edit
if(isset($_GET['type']) && $_GET['type'] == "edit_pembayaran"){
	$mode = $_GET['type'];
	$nota = $_GET['nota'];
	
	$hidden = "";
	$readonly = "readonly='readonly'";

	$res = mysql_query("select * from tb_pembayaran WHERE pembayaran_id = '$nota'");
	$foo = mysql_fetch_assoc($res);

	$grandtotal = $foo['pembayaran_billing'] - $foo['pembayaran_diskon'];
	$sisa = $grandtotal - $foo['pembayaran_deposit'];

	$pasienid = isset($foo['pasien_id']) ? $foo['pasien_id'] : 0;
	$pasquery = mysql_query("select * from tb_pemilik as a inner join tb_pasien as b on a.code_pemilik = b.code_pemilik inner join tb_histori_rawat_inap as c on a.code_pemilik = c.code_pemilik WHERE b.id_counter_pasien='".$pasienid."' order by c.tgl_berobat desc");
	$foo2 = mysql_fetch_array($pasquery);
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'new'){
	$status = "";
	$nota = "INV".date("YmdHis");
	$query=mysql_query("
		INSERT INTO tb_pembayaran 
		(
			pembayaran_diskon, 
			pembayaran_deposit, 
			pembayaran_kembalian, 
			pembayaran_date, 
			pembayaran_datecreated, 
			pembayaran_datemodified, 
			pembayaran_pembayaran, 
			pasien_id, 
			nota_pembayaran, 
			pembayaran_billing
		)VALUES(
			".$_POST['pembayaran_diskon'].",
			".$_POST['pembayaran_deposit'].",
			".$_POST['pembayaran_kembalian'].",
			'".date("Y-m-d H:i:s", strtotime($_POST['pembayaran_date']))."',
			'".date("Y-m-d H:i:s")."',
			'".date("Y-m-d H:i:s")."',
			".$_POST['pembayaran_pembayaran'].",
			".$_POST['pasien_id_real'].",
			'".$nota."',
			".$_POST['pembayaran_billing']."
		)");
	if($query){
		$status .="GLOBAL OK ";
		$idpembayaran = mysql_insert_id();
		$error = 0;
		$notasehat = isset($_POST['rawatsehat_nota']) ? $_POST['rawatsehat_nota'] : array();
		$tagihansehat = isset($_POST['rawatsehat_biaya']) ? $_POST['rawatsehat_biaya'] : array();
		$jasabarang_name_sehat = isset($_POST['rawatsehat_jasabarangname']) ? $_POST['rawatsehat_jasabarangname'] : array();
		$jasabarang_id_sehat = isset($_POST['rawatsehat_jasabarangid']) ? $_POST['rawatsehat_jasabarangid'] : array();
		$jasabarang_price_sehat = isset($_POST['rawatsehat_jasabarangprice']) ? $_POST['rawatsehat_jasabarangprice'] : array();
		$jasabarang_qty_sehat = isset($_POST['rawatsehat_jasabarangqty']) ? $_POST['rawatsehat_jasabarangqty'] : array();
		$jasabarang_date_sehat = isset($_POST['rawatsehat_jasabarangdate']) ? $_POST['rawatsehat_jasabarangdate'] : array();
		foreach($notasehat AS $key => $value){
			$querysehat = mysql_query("INSERT INTO tb_pembayaran_sehat 
			(
				pembayaran_id,
				nota,
				pembayaran_tagihan,
				detail_datecreated,
				detail_datemodified,
				jasa_barang_name,
				jasa_barang_id,
				jasa_barang_price,
				jasa_barang_qty,
				jasa_barang_date
			) VALUES (
				".$idpembayaran.",
				'".$value."',
				".$tagihansehat[$key].",
				'".date("Y-m-d H:i:s")."',
				'".date("Y-m-d H:i:s")."',
				'".$jasabarang_name_sehat[$key]."',
				".$jasabarang_id_sehat[$key].",
				".$tagihansehat[$key].",
				".$jasabarang_qty_sehat[$key].",
				'".$jasabarang_date_sehat[$key]."'
			)");
			
			$updatesehat = mysql_query("UPDATE tb_histori_rawat_sehat SET payment_status=1 WHERE nota='".$value."'");

			if(!$querysehat){
				$status .=" | ADD SEHAT FAILED ";
				$error += 1;
			}

			if(!$updatesehat){
				$status .=" | UPDATE SEHAT FAILED ";
				$error += 1;
			}
		}

		$notasakit = isset($_POST['rawatsakit_nota']) ? $_POST['rawatsakit_nota'] : array();
		$tagihansakit = isset($_POST['rawatsakit_biaya']) ? $_POST['rawatsakit_biaya'] : array();
		
		$jasabarang_name_sakit = isset($_POST['rawatsakit_jasabarangname']) ? $_POST['rawatsakit_jasabarangname'] : array();
		$jasabarang_id_sakit = isset($_POST['rawatsakit_jasabarangid']) ? $_POST['rawatsakit_jasabarangid'] : array();
		$jasabarang_price_sakit = isset($_POST['rawatsakit_jasabarangprice']) ? $_POST['rawatsakit_jasabarangprice'] : array();
		$jasabarang_qty_sakit = isset($_POST['rawatsakit_jasabarangqty']) ? $_POST['rawatsakit_jasabarangqty'] : array();
		$jasabarang_date_sakit = isset($_POST['rawatsakit_jasabarangdate']) ? $_POST['rawatsakit_jasabarangdate'] : array();
		foreach($notasakit AS $key => $value){
			$querysakit = mysql_query("INSERT INTO tb_pembayaran_sakit 
			(
				pembayaran_id,
				nota,
				pembayaran_tagihan,
				detail_datecreated,
				detail_datemodified,
				jasa_barang_name,
				jasa_barang_id,
				jasa_barang_price,
				jasa_barang_qty,
				jasa_barang_date
			) VALUES (
				".$idpembayaran.",
				'".$value."',
				".$tagihansakit[$key].",
				'".date("Y-m-d H:i:s")."',
				'".date("Y-m-d H:i:s")."',
				'".$jasabarang_name_sakit[$key]."',
				".$jasabarang_id_sakit[$key].",
				".$tagihansakit[$key].",
				".$jasabarang_qty_sakit[$key].",
				'".$jasabarang_date_sakit[$key]."'
			)");
			
			$updatesakit = mysql_query("UPDATE tb_histori_rawat_sakit SET payment_status=1 WHERE nota='".$value."'");

			if(!$querysakit){
				$status .=" | ADD SAKIT FAILED ";
				$error += 1;
			}

			if(!$updatesakit){
				$status .=" | UPDATE SAKIT FAILED ";
				$error +=1;
			}
		}

		$notainap = isset($_POST['rawatinap_nota']) ? $_POST['rawatinap_nota'] : array();
		$tagihaninap = isset($_POST['rawatinap_biaya']) ? $_POST['rawatinap_biaya'] : array();
		$jasabarang_name_inap = isset($_POST['rawatinap_jasabarangname']) ? $_POST['rawatinap_jasabarangname'] : array();
		$jasabarang_id_inap = isset($_POST['rawatinap_jasabarangid']) ? $_POST['rawatinap_jasabarangid'] : array();
		$jasabarang_price_inap = isset($_POST['rawatinap_jasabarangprice']) ? $_POST['rawatinap_jasabarangprice'] : array();
		$jasabarang_qty_inap = isset($_POST['rawatinap_jasabarangqty']) ? $_POST['rawatinap_jasabarangqty'] : array();
		$jasabarang_date_inap = isset($_POST['rawatinap_jasabarangdate']) ? $_POST['rawatinap_jasabarangdate'] : array();
		foreach($notainap AS $key => $value){
			$queryinap = mysql_query("INSERT INTO tb_pembayaran_inap 
			(
				pembayaran_id,
				nota,
				pembayaran_tagihan,
				detail_datecreated,
				detail_datemodified,
				jasa_barang_name,
				jasa_barang_id,
				jasa_barang_price,
				jasa_barang_qty,
				jasa_barang_date
			) VALUES (
				".$idpembayaran.",
				'".$value."',
				".$tagihaninap[$key].",
				'".date("Y-m-d H:i:s")."',
				'".date("Y-m-d H:i:s")."',
				'".$jasabarang_name_inap[$key]."',
				".$jasabarang_id_inap[$key].",
				".$tagihaninap[$key].",
				".$jasabarang_qty_inap[$key].",
				'".$jasabarang_date_inap[$key]."'
			)");

			$updateinap = mysql_query("UPDATE tb_histori_rawat_inap SET payment_status=1 WHERE nota='".$value."'");

			if(!$queryinap){
				$status .=" | ADD INAP FAILED ";
				$error += 1;
			}

			if(!$updateinap){
				$status .=" | UPDATE INAP FAILED ";
				$error += 1;
			}
		}

		if($error == 0){
			header('location: data_pembayaran.php');
		}else{
			$msg_error .= 'gagal simpan data pembayaran <br>';
		}
	}else{
		$msg_error .= 'gagal simpan data pembayaran <br>';
	}
	echo $status;
}

//save new data
if(isset($_POST['type']) && $_POST['type'] == 'edit'){
	$query = mysql_query("
		UPDATE tb_pembayaran SET 
		pembayaran_date='".date("Y-m-d H:i:s", strtotime($_POST['pembayaran_date']))."',
		pembayaran_diskon=".$_POST['pembayaran_diskon'].",
		pembayaran_kembalian=".$_POST['pembayaran_kembalian'].",
		pembayaran_pembayaran=".$_POST['pembayaran_pembayaran']." 
		WHERE pembayaran_id='".$_POST['pembayaran_id']."'"
	);
	if($query){
		header('location: data_pembayaran.php');
	}else{
		$msg_error .= 'gagal simpan data pembayaran <br>';
	}
}


include "site_header.php";
include "site_menu.php";

?>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<?php //form ?>
		<div class="page-title">

		</div>
		<div class="clearfix"></div>
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Pembayaran</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li><a class="close-link" href="data_pembayaran.php"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">
				<br />
				<form method="POST" action="" data-parsley-validate class="form-horizontal form-label-left" id="formbayar">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <small style="background:<?php echo ($msg_error !='') ? 'red' : 'green';?>; display:<?php echo ($msg_error !='') ? 'block' : 'none';?>" class="<?php echo ($msg_error !='') ? 'label label-info' : '';?>" ><i class="fa fa-exclamation-triangle"></i> <?php echo (isset($msg_error)) ? $msg_error : '';?></small>
						</div>
					</div>
				  <div class="form-group" style="display:none;">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Pemeriksaan
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="pembayaran_date" class="datetimepicker form-control" value="<?php echo isset($foo['pembayaran_date']) ? $foo['pembayaran_date'] : date('Y-m-d H:i:s') ?>">
                      <input type="hidden" id="nota"  class="form-control col-md-7 col-xs-12" name="nota" value="<?php echo (isset($nota)) ? $nota : '' ;?>">
                      <input type="hidden" id="pembayaran_id"  class="form-control col-md-7 col-xs-12" name="pembayaran_id" value="<?php echo (isset($foo['pembayaran_id'])) ? $foo['pembayaran_id'] : '' ;?>">
                      <input type="hidden"  name="type" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($mode == 'edit_pembayaran' ) ? 'edit' : 'new'; ?>">
                      <input type="hidden"  name="pasien_id_real" id="pasien_id" class="form-control col-md-7 col-xs-12" value="<?php echo isset($foo['pasien_id']) ? $foo['pasien_id'] : 0; ?>">
					</div>
				  </div>
				  <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pasien
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php 
							if(isset($_GET['type']) && $_GET['type'] == "edit_pembayaran"){
								?>
								<input type="text" readonly="readonly" name="pasien_id" class="form-control" value="<?php echo isset($foo2['id_counter_pasien']) ? $foo2['id_counter_pasien']." | ".$foo2['nama_hewan']." | ".$foo2['nama_pemilik'] : "" ?>">
								<?php
							}else{
								?>
								<select name="pasien_id" id="selectrekammedis" style="width:100%" required="required" class="selectpasien">

								</select>
								<?php
							}
						?>
						
					  </div>
				  </div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tagihan Rawat Sehat
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <table class="table table-responsive table-bordered">
					  	<thead>
						  <tr>
						  	<th>No</th>
							<th>Nota</th>
							<th>Tgl Pemeriksaan</th>
							<th>Jasa/Barang</th>
							<th>Harga</th>
							<th>Qty</th>
							<th>Tagihan</th>
						  </tr>
						</thead>
						<tbody id="tbrawatsehat">
							<?php
								if(isset($foo['pembayaran_id']) && !empty($foo['pembayaran_id'])){
									$dataquery = mysql_query("SELECT * FROM tb_pembayaran_sehat WHERE pembayaran_id='".$foo['pembayaran_id']."'");
									$no = 1;
									while($rquery = mysql_fetch_array($dataquery)){
										?>
										<tr>
											<td> 
												<?php echo $no ?>
												<input type='hidden' name='rawatsehat_nota[]' value='<?php echo $rquery['nota'] ?>'>
												<input type='hidden' name='rawatsehat_biaya[]' value='<?php echo $rquery['pembayaran_tagihan']?>'>
												<input type='hidden' name='rawatsehat_jasabarangid[]' value='<?php echo $rquery['jasa_barang_id'] ?>'>
            									<input type='hidden' name='rawatsehat_jasabarangname[]' value='<?php $rquery['jasa_barang_name'] ?>'>
            									<input type='hidden' name='rawatsehat_jasabarangqty[]' value='<?php $rquery['jasa_barang_qty'] ?>'>
												<input type='hidden' name='rawatsehat_jasabarangdate[]' value='<?php date("Y-m-d", strtotime($rquery['jasa_barang_date'])) ?>'>
											</td>
											<td><?php echo $rquery['nota'] ?></td>
											<td><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
											<td><?php echo $rquery['jasa_barang_name'] ?></td>
											<td> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
											<td> Rp <?php echo number_format($rquery['jasa_barang_qty']) ?></td>
											<td>Rp <?php number_format($rquery['jasa_barang_price']*$rquery['jasa_barang_qty']) ?></td>
										</tr>
										<?php
									}
								}
							?>
						</tbody>
					  </table>
					</div>
				  </div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tagihan Rawat Sakit
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <table class="table table-responsive table-bordered">
					  	<thead>
						  <tr>
						  	<th>No</th>
							<th>Nota</th>
							<th>Tgl Pemeriksaan</th>
							<th>Jasa/Barang</th>
							<th>Harga</th>
							<th>Qty</th>
							<th>Tagihan</th>
						  </tr>
						</thead>
						<tbody id="tbrawatsakit">
							<?php
								if(isset($foo['pembayaran_id']) && !empty($foo['pembayaran_id'])){
									$dataquery = mysql_query("SELECT * FROM tb_pembayaran_sakit WHERE pembayaran_id='".$foo['pembayaran_id']."'");
									$no = 1;
									while($rquery = mysql_fetch_array($dataquery)){
										?>
										<tr>
											<td> 
												<?php echo $no ?>
												<input type='hidden' name='rawatsakit_nota[]' value='<?php echo $rquery['nota'] ?>'>
												<input type='hidden' name='rawatsakit_biaya[]' value='<?php echo $rquery['pembayaran_tagihan']?>'>
												<input type='hidden' name='rawatsakit_jasabarangid[]' value='<?php echo $rquery['jasa_barang_id'] ?>'>
            									<input type='hidden' name='rawatsakit_jasabarangname[]' value='<?php $rquery['jasa_barang_name'] ?>'>
            									<input type='hidden' name='rawatsakit_jasabarangqty[]' value='<?php $rquery['jasa_barang_qty'] ?>'>
												<input type='hidden' name='rawatsakit_jasabarangdate[]' value='<?php date("Y-m-d", strtotime($rquery['jasa_barang_date'])) ?>'>
											</td>
											<td><?php echo $rquery['nota'] ?></td>
											<td><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
											<td><?php echo $rquery['jasa_barang_name'] ?></td>
											<td> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
											<td><?php echo number_format($rquery['jasa_barang_qty']) ?></td>
											<td>Rp <?php echo number_format($rquery['jasa_barang_price']*$rquery['jasa_barang_qty']) ?></td>
										</tr>
										<?php
									}
								}
							?>
						</tbody>
					  </table>
					</div>
				  </div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tagihan Rawat Inap
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <table class="table table-responsive table-bordered">
					  	<thead>
						  <tr>
						  	<th>No</th>
							<th>Nota</th>
							<th>Tgl Pemeriksaan</th>
							<th>Jasa/Barang</th>
							<th>Harga</th>
							<th>Qty</th>
							<th>Tagihan</th>
						  </tr>
						</thead>
						<tbody id="tbrawatinap">
							<?php
								if(isset($foo['pembayaran_id']) && !empty($foo['pembayaran_id'])){
									$dataquery = mysql_query("SELECT * FROM tb_pembayaran_inap WHERE pembayaran_id='".$foo['pembayaran_id']."'");
									$no = 1;
									while($rquery = mysql_fetch_array($dataquery)){
										?>
										<tr>
											<td> 
												<?php echo $no ?>
												<input type='hidden' name='rawatinap_nota[]' value='<?php echo $rquery['nota'] ?>'>
												<input type='hidden' name='rawatinap_biaya[]' value='<?php echo $rquery['pembayaran_tagihan']?>'>
												<input type='hidden' name='rawatinap_jasabarangid[]' value='<?php echo $rquery['jasa_barang_id'] ?>'>
            									<input type='hidden' name='rawatinap_jasabarangname[]' value='<?php $rquery['jasa_barang_name'] ?>'>
            									<input type='hidden' name='rawatinap_jasabarangqty[]' value='<?php $rquery['jasa_barang_qty'] ?>'>
												<input type='hidden' name='rawatinap_jasabarangdate[]' value='<?php date("Y-m-d", strtotime($rquery['jasa_barang_date'])) ?>'>
											</td>
											<td><?php echo $rquery['nota'] ?></td>
											<td><?php echo date("d F Y", strtotime($rquery['jasa_barang_date'])) ?></td>
											<td><?php echo $rquery['jasa_barang_name'] ?></td>
											<td> Rp <?php echo number_format($rquery['jasa_barang_price'])?></td>
											<td><?php echo number_format($rquery['jasa_barang_qty']) ?></td>
											<td>Rp <?php echo number_format($rquery['jasa_barang_price']*$rquery['jasa_barang_qty']) ?></td>
										</tr>
										<?php
									}
								}
							?>
						</tbody>
					  </table>
					</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"> Subtotal
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" readonly="readonly" name="pembayaran_billing" id="pembayaran_billing" class="form-control" value="<?php echo isset($foo['pembayaran_billing']) ? $foo['pembayaran_billing'] : 0 ?>">
					</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Diskon
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" <?php echo $readonly ?> id="pembayaran_diskon" name="pembayaran_diskon" class="form-control autocalc" value="<?php echo isset($foo['pembayaran_diskon']) ? $foo['pembayaran_diskon'] : 0 ?>">
					</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Deposit
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" <?php echo $readonly ?>  id="pembayaran_deposit" name="pembayaran_deposit" class="form-control autocalc" value="<?php echo isset($foo['pembayaran_deposit']) ? $foo['pembayaran_deposit'] : 0 ?>">
					</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sisa Tagihan
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" <?php echo $readonly ?>  readonly="readonly" id="pembayaran_total" name="pembayaran_total" class="form-control autocalc" value="<?php echo isset($foo['pembayaran_billing']) ? $foo['pembayaran_billing']-$foo['pembayaran_diskon']-$foo['pembayaran_deposit'] : 0 ?>">
					</div>
					</div>
					<div class="form-group" style="<?php echo $hidden ?>">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pembayaran Sisa Tagihan
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="pembayaran_pembayaran" name="pembayaran_pembayaran" class="form-control autocalc" value="<?php echo isset($foo['pembayaran_pembayaran']) ? $foo['pembayaran_pembayaran'] : 0 ?>">
					</div>
					</div>
					<div class="form-group" style="display:none;">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kembalian
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <input type="text" id="pembayaran_kembalian" name="pembayaran_kembalian" class="form-control autocalc" value="<?php echo isset($foo['pembayaran_kembalian']) ? $foo['pembayaran_kembalian'] : 0 ?>">
					</div>
					</div>
				  <!--div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Obat
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <textarea class="form-control" name="obat">
					  	<?php echo (isset($obat))? $obat : '';?>
					  </textarea>
					</div>
				</div-->

				  <div class="ln_solid"></div>
				  <div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <a href="data_pembayaran.php?nota=<?php echo $nota ?>"><button type="button" class="btn btn-primary">Cancel</button></a>
					  <button type="submit" class="btn btn-success">Save</button>
					  <a href="print_pembayaran.php?code_pemilik=<?php echo isset($foo['pembayaran_id']) ? $foo['pembayaran_id'] : 0 ?>" class="btn btn-default">Cetak Nota Pembayaran</a>
					</div>
				  </div>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div><!--page content-->
<?php
include "site_footer.php";
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.datapicker').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true
		});
	});
</script>
